package mme.math.glmatrix;

import mme.math.glmatrix.GLMatrix;

import mme.math.glmatrix.Quat2;
import mme.math.glmatrix.Quat;
import mme.math.glmatrix.Vec3;

import mme.math.glmatrix.QuatTools;

/**
 * Dual Quaternion<br>
 * Format: [real, dual]<br>
 * Quaternion format: XYZW<br>
 * Make sure to have normalized dual quaternions, otherwise the functions may not work as intended.<br>
 * @module Quat2
 */
class Quat2Tools {

/*

//
// GENERATORS in Quat2 class
//

Quat2.fromValues( ?out : Quat2, x1 : Float, y1 : Float, z1 : Float, w1 : Float, x2 : Float, y2 : Float, z2 : Float, w2 : Float ) : Quat2;
Quat2.fromRotationTranslationValues( ?out : Quat2, x1 : Float, y1 : Float, z1 : Float, w1 : Float, x2 : Float, y2 : Float, z2 : Float ) : Quat2;
Quat2.fromRotationTranslation( ?out : Quat2, q : Quat, t : Vec3 ) : Quat2;
Quat2.fromTranslation( ?out : Quat2, t : Vec3 ) : Quat2;
Quat2.fromRotation( ?out : Quat2, q : Quat ) : Quat2;
Quat2.fromMat4( ?out : Quat2, a : Mat4 ) : Quat2;


//
// GENERATORS
//
create() : Quat2;
quat2.clone( a : Quat2 ) : Quat2;
quat2.copy( a : Quat2, ?out : Quat2 ) : Quat2;
?quat2.identity( ?out : Quat2 ) : Quat2;

lerp( t : Float, a : Quat2, b : Quat2, ?out : Quat2 ) : Quat2;


//
// EDIT
//
quat2.set( out : Quat2, x1 : Float, y1 : Float, z1 : Float, w1 : Float, x2 : Float, y2 : Float, z2 : Float, w2 : Float ) : Quat2;
quat2.setReal( out : Quat2, a : Quat ) : Quat2;
quat2.setDual( out : Quat2, q : Quat ) : Quat2;


//
// OPERATORS
//

// unary
//
quat2.invert( a : Quat2, ?out : Quat2 ) : Quat2;
quat2.conjugate( a : Quat2, ?out : Quat2 ) : Quat2;
quat2.normalize( a : Quat2, ?out : Quat2 ) : Quat2;



// unary, non-Quat2 result
//
quat2.length( a : Quat2 ) : Float;
quat2.squaredLength( a : Quat2 ) : Float;
quat2.str( a : Quat2 ) : String;

quat2.getReal( a : Quat2, ?out : Quat ) : Quat;
quat2.getDual( a : Quat2, ?out : Quat ) : Quat;
quat2.getTranslation( a : Quat2, ?out : Vec3 ) : Vec3;


// binary
//
quat2.add( a : Quat2, b : Quat2, ?out : Quat2 ) : Quat2;
quat2.multiply( a : Quat2, b : Quat2, ?out : Quat2 ) : Quat2;


// binary, non-Quat2 arg
//
quat2.scale( a : Quat2, b : Float, ?out : Quat2 ) : Quat2;
quat2.translate( a : Quat2, v : Vec3, ?out : Quat2 ) : Quat2;
quat2.rotateX( a : Quat2, rad : Float, ?out : Quat2 ) : Quat2;
quat2.rotateY( a : Quat2, rad : Float, ?out : Quat2 ) : Quat2;
quat2.rotateZ( a : Quat2, rad : Float, ?out : Quat2 ) : Quat2;
quat2.rotateByQuatAppend( a : Quat2, q : Quat, ?out : Quat2 ) : Quat2;
quat2.rotateByQuatPrepend( a : Quat2, q : Quat, ?out : Quat2 ) : Quat2;


// binary, non-Quat2 result
//
quat2.dot( a : Quat2, b : Quat2 ) : Float;
quat2.exactEquals( a : Quat2, b : Quat2 ) : Bool;
quat2.equals( a : Quat2, b : Quat2 ) : Bool;


// ternary
//
quat2.rotateAroundAxis( a : Quat2, axis : Vec3, rad : Float, ?out : Quat2 ) : Quat2;

*/

    /**
    * Creates a new identity dual quat
    *
    * @returns {Quat2} a new dual quaternion [real -> rotation, dual -> translation]
    */
    public static inline function create() : Quat2 {
    var dq = new Quat2();
    return dq;
    }

    /**
    * Creates a new quat initialized with values from an existing quaternion
    *
    * @param {Quat2} a dual quaternion to clone
    * @returns {Quat2} new dual quaternion
    * @function
    */
    public static function clone( a : Quat2 ) : Quat2 {
    var dq = new Quat2();
    dq[0] = a[0];
    dq[1] = a[1];
    dq[2] = a[2];
    dq[3] = a[3];
    dq[4] = a[4];
    dq[5] = a[5];
    dq[6] = a[6];
    dq[7] = a[7];
    return dq;
    }

    /**
    * Copy the values from one dual quat to another
    *
    * @param {Quat2} out the receiving dual quaternion
    * @param {Quat2} a the source dual quaternion
    * @returns {Quat2} out
    * @function
    */
    public static function copy( a : Quat2, ?out : Quat2 ) : Quat2 {
    if( out == null ) out = create();
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
    out[4] = a[4];
    out[5] = a[5];
    out[6] = a[6];
    out[7] = a[7];
    return out;
    }

    /**
    * Set a dual quat to the identity dual quaternion
    *
    * @param {Quat2} out the receiving quaternion
    * @returns {Quat2} out
    */
    public static function identity( out : Quat2 ) : Quat2 {
    out[0] = 0;
    out[1] = 0;
    out[2] = 0;
    out[3] = 1;
    out[4] = 0;
    out[5] = 0;
    out[6] = 0;
    out[7] = 0;
    return out;
    }

    /**
    * Set the components of a dual quat to the given values
    *
    * @param {Quat2} out the receiving quaternion
    * @param {Float} x1 X component
    * @param {Float} y1 Y component
    * @param {Float} z1 Z component
    * @param {Float} w1 W component
    * @param {Float} x2 X component
    * @param {Float} y2 Y component
    * @param {Float} z2 Z component
    * @param {Float} w2 W component
    * @returns {Quat2} out
    * @function
    */
    public static function set( out : Quat2, x1 : Float, y1 : Float, z1 : Float, w1 : Float, x2 : Float, y2 : Float, z2 : Float, w2 : Float ) : Quat2 {
    out[0] = x1;
    out[1] = y1;
    out[2] = z1;
    out[3] = w1;

    out[4] = x2;
    out[5] = y2;
    out[6] = z2;
    out[7] = w2;
    return out;
    }

    /**
    * Gets the real part of a dual quat
    * @param  {Quat} out real part
    * @param  {Quat2} a Dual Quaternion
    * @return {quat} real part
    */
    // public static var getReal = QuatTools.copy;
    public static function getReal( a : Quat2, ?out : Quat ) : Quat {
    if( out == null ) out = QuatTools.create();
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
    return out;
    }

    /**
    * Gets the dual part of a dual quat
    * @param  {Quat} out dual part
    * @param  {Quat2} a Dual Quaternion
    * @return {quat} dual part
    */
    public static function getDual( a : Quat2, ?out : Quat ) : Quat {
    if( out == null ) out = QuatTools.create();
    out[0] = a[4];
    out[1] = a[5];
    out[2] = a[6];
    out[3] = a[7];
    return out;
    }

    /**
    * Set the real component of a dual quat to the given quaternion
    *
    * @param {Quat2} out the receiving quaternion
    * @param {Quat} q a quaternion representing the real part
    * @returns {Quat2} out
    * @function
    */
    // public static var setReal = QuatTools.copy;
    public static function setReal( out : Quat2, a : Quat ) : Quat2 {
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
    return out;
    }

    /**
    * Set the dual component of a dual quat to the given quaternion
    *
    * @param {Quat2} out the receiving quaternion
    * @param {Quat} q a quaternion representing the dual part
    * @returns {Quat2} out
    * @function
    */
    public static function setDual( out : Quat2, q : Quat ) : Quat2 {
    out[4] = q[0];
    out[5] = q[1];
    out[6] = q[2];
    out[7] = q[3];
    return out;
    }

    /**
    * Gets the translation of a normalized dual quat
    * @param  {Vec3} out translation
    * @param  {Quat2} a Dual Quaternion to be decomposed
    * @return {Vec3} translation
    */
    public static function getTranslation( a : Quat2, ?out : Vec3 ) : Vec3 {
    if( out == null ) out = Vec3Tools.create();
    var ax = a[4],
        ay = a[5],
        az = a[6],
        aw = a[7],
        bx = -a[0],
        by = -a[1],
        bz = -a[2],
        bw = a[3];
    out[0] = (ax * bw + aw * bx + ay * bz - az * by) * 2;
    out[1] = (ay * bw + aw * by + az * bx - ax * bz) * 2;
    out[2] = (az * bw + aw * bz + ax * by - ay * bx) * 2;
    return out;
    }

    /**
    * Translates a dual quat by the given vector
    *
    * @param {Quat2} out the receiving dual quaternion
    * @param {Quat2} a the dual quaternion to translate
    * @param {Vec3} v vector to translate by
    * @returns {Quat2} out
    */
    public static function translate( a : Quat2, v : Vec3, ?out : Quat2 ) : Quat2 {
    if( out == null ) out = create();
    var ax1 = a[0],
        ay1 = a[1],
        az1 = a[2],
        aw1 = a[3],
        bx1 = v[0] * 0.5,
        by1 = v[1] * 0.5,
        bz1 = v[2] * 0.5,
        ax2 = a[4],
        ay2 = a[5],
        az2 = a[6],
        aw2 = a[7];
    out[0] = ax1;
    out[1] = ay1;
    out[2] = az1;
    out[3] = aw1;
    out[4] = aw1 * bx1 + ay1 * bz1 - az1 * by1 + ax2;
    out[5] = aw1 * by1 + az1 * bx1 - ax1 * bz1 + ay2;
    out[6] = aw1 * bz1 + ax1 * by1 - ay1 * bx1 + az2;
    out[7] = -ax1 * bx1 - ay1 * by1 - az1 * bz1 + aw2;
    return out;
    }

    /** Helper function for rotateX */
    private static inline function rotateXReal( a : Quat2, rad : Float, out : Quat2 ) : Quat2 {
    rad *= 0.5;

    var ax = a[0], ay = a[1], az = a[2], aw = a[3];
    var bx = Math.sin(rad), bw = Math.cos(rad);

    out[0] = ax * bw + aw * bx;
    out[1] = ay * bw + az * bx;
    out[2] = az * bw - ay * bx;
    out[3] = aw * bw - ax * bx;
    return out;
    }

    /**
    * Rotates a dual quat around the X axis
    *
    * @param {Quat2} out the receiving dual quaternion
    * @param {Quat2} a the dual quaternion to rotate
    * @param {Float} rad how far should the rotation be
    * @returns {Quat2} out
    */
    public static function rotateX( a : Quat2, rad : Float, ?out : Quat2 ) : Quat2 {
    if( out == null ) out = create();
    var bx = -a[0],
        by = -a[1],
        bz = -a[2],
        bw = a[3],
        ax = a[4],
        ay = a[5],
        az = a[6],
        aw = a[7],
        ax1 = ax * bw + aw * bx + ay * bz - az * by,
        ay1 = ay * bw + aw * by + az * bx - ax * bz,
        az1 = az * bw + aw * bz + ax * by - ay * bx,
        aw1 = aw * bw - ax * bx - ay * by - az * bz;
    // QuatTools.rotateX(out, a, rad);
    rotateXReal(a, rad, out);
    bx = out[0];
    by = out[1];
    bz = out[2];
    bw = out[3];
    out[4] = ax1 * bw + aw1 * bx + ay1 * bz - az1 * by;
    out[5] = ay1 * bw + aw1 * by + az1 * bx - ax1 * bz;
    out[6] = az1 * bw + aw1 * bz + ax1 * by - ay1 * bx;
    out[7] = aw1 * bw - ax1 * bx - ay1 * by - az1 * bz;
    return out;
    }

    /** Helper function for rotateY */
    private static inline function rotateYReal( a : Quat2, rad : Float, out : Quat2 ) : Quat2 {
    rad *= 0.5;

    var ax = a[0], ay = a[1], az = a[2], aw = a[3];
    var by = Math.sin(rad), bw = Math.cos(rad);

    out[0] = ax * bw - az * by;
    out[1] = ay * bw + aw * by;
    out[2] = az * bw + ax * by;
    out[3] = aw * bw - ay * by;
    return out;
    }

    /**
    * Rotates a dual quat around the Y axis
    *
    * @param {Quat2} out the receiving dual quaternion
    * @param {Quat2} a the dual quaternion to rotate
    * @param {Float} rad how far should the rotation be
    * @returns {Quat2} out
    */
    public static function rotateY( a : Quat2, rad : Float, ?out : Quat2 ) : Quat2 {
    if( out == null ) out = create();
    var bx = -a[0],
        by = -a[1],
        bz = -a[2],
        bw = a[3],
        ax = a[4],
        ay = a[5],
        az = a[6],
        aw = a[7],
        ax1 = ax * bw + aw * bx + ay * bz - az * by,
        ay1 = ay * bw + aw * by + az * bx - ax * bz,
        az1 = az * bw + aw * bz + ax * by - ay * bx,
        aw1 = aw * bw - ax * bx - ay * by - az * bz;
    // QuatTools.rotateY(out, a, rad);
    rotateYReal(a, rad, out);
    bx = out[0];
    by = out[1];
    bz = out[2];
    bw = out[3];
    out[4] = ax1 * bw + aw1 * bx + ay1 * bz - az1 * by;
    out[5] = ay1 * bw + aw1 * by + az1 * bx - ax1 * bz;
    out[6] = az1 * bw + aw1 * bz + ax1 * by - ay1 * bx;
    out[7] = aw1 * bw - ax1 * bx - ay1 * by - az1 * bz;
    return out;
    }

    /** Helper function for rotateZ */
    private static inline function rotateZReal( a : Quat2, rad : Float, out : Quat2 ) : Quat2 {
    rad *= 0.5;

    var ax = a[0], ay = a[1], az = a[2], aw = a[3];
    var bz = Math.sin(rad), bw = Math.cos(rad);

    out[0] = ax * bw + ay * bz;
    out[1] = ay * bw - ax * bz;
    out[2] = az * bw + aw * bz;
    out[3] = aw * bw - az * bz;
    return out;
    }

    /**
    * Rotates a dual quat around the Z axis
    *
    * @param {Quat2} out the receiving dual quaternion
    * @param {Quat2} a the dual quaternion to rotate
    * @param {Float} rad how far should the rotation be
    * @returns {Quat2} out
    */
    public static function rotateZ( a : Quat2, rad : Float, ?out : Quat2 ) : Quat2 {
    if( out == null ) out = create();
    var bx = -a[0],
        by = -a[1],
        bz = -a[2],
        bw = a[3],
        ax = a[4],
        ay = a[5],
        az = a[6],
        aw = a[7],
        ax1 = ax * bw + aw * bx + ay * bz - az * by,
        ay1 = ay * bw + aw * by + az * bx - ax * bz,
        az1 = az * bw + aw * bz + ax * by - ay * bx,
        aw1 = aw * bw - ax * bx - ay * by - az * bz;
    // QuatTools.rotateZ(out, a, rad);
    rotateZReal(a, rad, out);
    bx = out[0];
    by = out[1];
    bz = out[2];
    bw = out[3];
    out[4] = ax1 * bw + aw1 * bx + ay1 * bz - az1 * by;
    out[5] = ay1 * bw + aw1 * by + az1 * bx - ax1 * bz;
    out[6] = az1 * bw + aw1 * bz + ax1 * by - ay1 * bx;
    out[7] = aw1 * bw - ax1 * bx - ay1 * by - az1 * bz;
    return out;
    }

    /**
    * Rotates a dual quat by a given quaternion (a * q)
    *
    * @param {Quat2} out the receiving dual quaternion
    * @param {Quat2} a the dual quaternion to rotate
    * @param {Quat} q quaternion to rotate by
    * @returns {Quat2} out
    */
    public static function rotateByQuatAppend( a : Quat2, q : Quat, ?out : Quat2 ) : Quat2 {
    if( out == null ) out = create();
    var qx = q[0],
        qy = q[1],
        qz = q[2],
        qw = q[3],
        ax = a[0],
        ay = a[1],
        az = a[2],
        aw = a[3];

    out[0] = ax * qw + aw * qx + ay * qz - az * qy;
    out[1] = ay * qw + aw * qy + az * qx - ax * qz;
    out[2] = az * qw + aw * qz + ax * qy - ay * qx;
    out[3] = aw * qw - ax * qx - ay * qy - az * qz;
    ax = a[4];
    ay = a[5];
    az = a[6];
    aw = a[7];
    out[4] = ax * qw + aw * qx + ay * qz - az * qy;
    out[5] = ay * qw + aw * qy + az * qx - ax * qz;
    out[6] = az * qw + aw * qz + ax * qy - ay * qx;
    out[7] = aw * qw - ax * qx - ay * qy - az * qz;
    return out;
    }

    /**
    * Rotates a dual quat by a given quaternion (q * a)
    *
    * @param {Quat2} out the receiving dual quaternion
    * @param {Quat} q quaternion to rotate by
    * @param {Quat2} a the dual quaternion to rotate
    * @returns {Quat2} out
    */
    public static function rotateByQuatPrepend( a : Quat2, q : Quat, ?out : Quat2 ) : Quat2 {
    if( out == null ) out = create();
    var qx = q[0],
        qy = q[1],
        qz = q[2],
        qw = q[3],
        bx = a[0],
        by = a[1],
        bz = a[2],
        bw = a[3];

    out[0] = qx * bw + qw * bx + qy * bz - qz * by;
    out[1] = qy * bw + qw * by + qz * bx - qx * bz;
    out[2] = qz * bw + qw * bz + qx * by - qy * bx;
    out[3] = qw * bw - qx * bx - qy * by - qz * bz;
    bx = a[4];
    by = a[5];
    bz = a[6];
    bw = a[7];
    out[4] = qx * bw + qw * bx + qy * bz - qz * by;
    out[5] = qy * bw + qw * by + qz * bx - qx * bz;
    out[6] = qz * bw + qw * bz + qx * by - qy * bx;
    out[7] = qw * bw - qx * bx - qy * by - qz * bz;
    return out;
    }

    /**
    * Rotates a dual quat around a given axis. Does the normalisation automatically
    *
    * @param {Quat2} out the receiving dual quaternion
    * @param {Quat2} a the dual quaternion to rotate
    * @param {Vec3} axis the axis to rotate around
    * @param {Float} rad how far the rotation should be
    * @returns {Quat2} out
    */
    public static function rotateAroundAxis( a : Quat2, axis : Vec3, rad : Float, ?out : Quat2 ) : Quat2 {
    if( out == null ) out = create();
    //Special case for rad = 0
    if (Math.abs(rad) < GLMatrix.EPSILON) {
        return copy(out, a);
    }
    var axisLength = Math.sqrt(axis[0] * axis[0] + axis[1] * axis[1] + axis[2] * axis[2]);

    rad = rad * 0.5;
    var s = Math.sin(rad);
    var bx = s * axis[0] / axisLength;
    var by = s * axis[1] / axisLength;
    var bz = s * axis[2] / axisLength;
    var bw = Math.cos(rad);

    var ax1 = a[0],
        ay1 = a[1],
        az1 = a[2],
        aw1 = a[3];
    out[0] = ax1 * bw + aw1 * bx + ay1 * bz - az1 * by;
    out[1] = ay1 * bw + aw1 * by + az1 * bx - ax1 * bz;
    out[2] = az1 * bw + aw1 * bz + ax1 * by - ay1 * bx;
    out[3] = aw1 * bw - ax1 * bx - ay1 * by - az1 * bz;

    var ax = a[4],
        ay = a[5],
        az = a[6],
        aw = a[7];
    out[4] = ax * bw + aw * bx + ay * bz - az * by;
    out[5] = ay * bw + aw * by + az * bx - ax * bz;
    out[6] = az * bw + aw * bz + ax * by - ay * bx;
    out[7] = aw * bw - ax * bx - ay * by - az * bz;

    return out;
    }

    /**
    * Adds two dual quat's
    *
    * @param {Quat2} out the receiving dual quaternion
    * @param {Quat2} a the first operand
    * @param {Quat2} b the second operand
    * @returns {Quat2} out
    * @function
    */
    public static function add( a : Quat2, b : Quat2, ?out : Quat2 ) : Quat2 {
    if( out == null ) out = create();
    out[0] = a[0] + b[0];
    out[1] = a[1] + b[1];
    out[2] = a[2] + b[2];
    out[3] = a[3] + b[3];
    out[4] = a[4] + b[4];
    out[5] = a[5] + b[5];
    out[6] = a[6] + b[6];
    out[7] = a[7] + b[7];
    return out;
    }

    /**
    * Multiplies two dual quat's
    *
    * @param {Quat2} out the receiving dual quaternion
    * @param {Quat2} a the first operand
    * @param {Quat2} b the second operand
    * @returns {Quat2} out
    */
    public static function multiply( a : Quat2, b : Quat2, ?out : Quat2 ) : Quat2 {
    if( out == null ) out = create();
    var ax0 = a[0],
        ay0 = a[1],
        az0 = a[2],
        aw0 = a[3],
        bx1 = b[4],
        by1 = b[5],
        bz1 = b[6],
        bw1 = b[7],
        ax1 = a[4],
        ay1 = a[5],
        az1 = a[6],
        aw1 = a[7],
        bx0 = b[0],
        by0 = b[1],
        bz0 = b[2],
        bw0 = b[3];
    out[0] = ax0 * bw0 + aw0 * bx0 + ay0 * bz0 - az0 * by0;
    out[1] = ay0 * bw0 + aw0 * by0 + az0 * bx0 - ax0 * bz0;
    out[2] = az0 * bw0 + aw0 * bz0 + ax0 * by0 - ay0 * bx0;
    out[3] = aw0 * bw0 - ax0 * bx0 - ay0 * by0 - az0 * bz0;
    out[4] = ax0 * bw1 + aw0 * bx1 + ay0 * bz1 - az0 * by1 + ax1 * bw0 + aw1 * bx0 + ay1 * bz0 - az1 * by0;
    out[5] = ay0 * bw1 + aw0 * by1 + az0 * bx1 - ax0 * bz1 + ay1 * bw0 + aw1 * by0 + az1 * bx0 - ax1 * bz0;
    out[6] = az0 * bw1 + aw0 * bz1 + ax0 * by1 - ay0 * bx1 + az1 * bw0 + aw1 * bz0 + ax1 * by0 - ay1 * bx0;
    out[7] = aw0 * bw1 - ax0 * bx1 - ay0 * by1 - az0 * bz1 + aw1 * bw0 - ax1 * bx0 - ay1 * by0 - az1 * bz0;
    return out;
    }

    /**
    * Alias for {@link Quat2.multiply}
    * @function
    */
    public static var mul = multiply;

    /**
    * Scales a dual quat by a scalar Float
    *
    * @param {Quat2} out the receiving dual quat
    * @param {Quat2} a the dual quat to scale
    * @param {Float} b amount to scale the dual quat by
    * @returns {Quat2} out
    * @function
    */
    public static function scale( a : Quat2, b : Float, ?out : Quat2 ) : Quat2 {
    if( out == null ) out = create();
    out[0] = a[0] * b;
    out[1] = a[1] * b;
    out[2] = a[2] * b;
    out[3] = a[3] * b;
    out[4] = a[4] * b;
    out[5] = a[5] * b;
    out[6] = a[6] * b;
    out[7] = a[7] * b;
    return out;
    }

    /**
    * Calculates the dot product of two dual quat's (The dot product of the real parts)
    *
    * @param {Quat2} a the first operand
    * @param {Quat2} b the second operand
    * @returns {Float} dot product of a and b
    * @function
    */
    // public static var dot = QuatTools.dot;
    public static function dot( a : Quat2, b : Quat2 ) : Float {
    return a[0] * b[0] + a[1] * b[1] + a[2] * b[2] + a[3] * b[3];
    }

    /**
    * Performs a linear interpolation between two dual quats's
    * NOTE: The resulting dual quaternions won't always be normalized (The error is most noticeable when t = 0.5)
    *
    * @param {Quat2} out the receiving dual quat
    * @param {Quat2} a the first operand
    * @param {Quat2} b the second operand
    * @param {Float} t interpolation amount, in the range [0-1], between the two inputs
    * @returns {Quat2} out
    */
    public static function lerp( t : Float, a : Quat2, b : Quat2, ?out : Quat2 ) : Quat2 {
    if( out == null ) out = create();
    var mt = 1 - t;
    if (dot(a, b) < 0) t = -t;

    out[0] = a[0] * mt + b[0] * t;
    out[1] = a[1] * mt + b[1] * t;
    out[2] = a[2] * mt + b[2] * t;
    out[3] = a[3] * mt + b[3] * t;
    out[4] = a[4] * mt + b[4] * t;
    out[5] = a[5] * mt + b[5] * t;
    out[6] = a[6] * mt + b[6] * t;
    out[7] = a[7] * mt + b[7] * t;

    return out;
    }

    /**
    * Calculates the inverse of a dual quat. If they are normalized, conjugate is cheaper
    *
    * @param {Quat2} out the receiving dual quaternion
    * @param {Quat2} a dual quat to calculate inverse of
    * @returns {Quat2} out
    */
    public static function invert( a : Quat2, ?out : Quat2 ) : Quat2 {
    if( out == null ) out = create();
    var sqlen = squaredLength(a);
    out[0] = -a[0] / sqlen;
    out[1] = -a[1] / sqlen;
    out[2] = -a[2] / sqlen;
    out[3] = a[3] / sqlen;
    out[4] = -a[4] / sqlen;
    out[5] = -a[5] / sqlen;
    out[6] = -a[6] / sqlen;
    out[7] = a[7] / sqlen;
    return out;
    }

    /**
    * Calculates the conjugate of a dual quat
    * If the dual quaternion is normalized, this function is faster than Quat2.inverse and produces the same result.
    *
    * @param {Quat2} out the receiving quaternion
    * @param {Quat2} a quat to calculate conjugate of
    * @returns {Quat2} out
    */
    public static function conjugate( a : Quat2, ?out : Quat2 ) : Quat2 {
    if( out == null ) out = create();
    out[0] = -a[0];
    out[1] = -a[1];
    out[2] = -a[2];
    out[3] = a[3];
    out[4] = -a[4];
    out[5] = -a[5];
    out[6] = -a[6];
    out[7] = a[7];
    return out;
    }

    /**
    * Calculates the length of a dual quat
    *
    * @param {Quat2} a dual quat to calculate length of
    * @returns {Float} length of a
    * @function
    */
    // public static var length = QuatTools.length;
    public static function length( a : Quat2 ) : Float {
    var x = a[0];
    var y = a[1];
    var z = a[2];
    var w = a[3];
    return Math.sqrt(x*x + y*y + z*z + w*w);
    }

    /**
    * Alias for {@link Quat2.length}
    * @function
    */
    public static var len = length;

    /**
    * Calculates the squared length of a dual quat
    *
    * @param {Quat2} a dual quat to calculate squared length of
    * @returns {Float} squared length of a
    * @function
    */
    // public static var squaredLength = QuatTools.squaredLength;
    public static function squaredLength( a : Quat2 ) : Float {
    var x = a[0];
    var y = a[1];
    var z = a[2];
    var w = a[3];
    return x*x + y*y + z*z + w*w;
    }

    /**
    * Alias for {@link Quat2.squaredLength}
    * @function
    */
    public static var sqrLen = squaredLength;

    /**
    * Normalize a dual quat
    *
    * @param {Quat2} out the receiving dual quaternion
    * @param {Quat2} a dual quaternion to normalize
    * @returns {Quat2} out
    * @function
    */
    public static function normalize( a : Quat2, ?out : Quat2 ) : Quat2 {
    if( out == null ) out = create();
    var magnitude = squaredLength(a);
    if (magnitude > 0) {
        magnitude = Math.sqrt(magnitude);

        var a0 = a[0] / magnitude;
        var a1 = a[1] / magnitude;
        var a2 = a[2] / magnitude;
        var a3 = a[3] / magnitude;

        var b0 = a[4];
        var b1 = a[5];
        var b2 = a[6];
        var b3 = a[7];

        var a_dot_b = (a0 * b0) + (a1 * b1) + (a2 * b2) + (a3 * b3);

        out[0] = a0;
        out[1] = a1;
        out[2] = a2;
        out[3] = a3;

        out[4] = (b0 - (a0 * a_dot_b)) / magnitude;
        out[5] = (b1 - (a1 * a_dot_b)) / magnitude;
        out[6] = (b2 - (a2 * a_dot_b)) / magnitude;
        out[7] = (b3 - (a3 * a_dot_b)) / magnitude;
    }
    return out;
    }

    /**
    * Returns a string representation of a dual quatenion
    *
    * @param {Quat2} a dual quaternion to represent as a string
    * @returns {String} string representation of the dual quat
    */
    public static function str( a : Quat2 ) : String {
    return 'Quat2(' + a[0] + ', ' + a[1] + ', ' + a[2] + ', ' + a[3] + ', ' +
        a[4] + ', ' + a[5] + ', ' + a[6] + ', ' + a[7] + ')';
    }

    /**
    * Returns whether or not the dual quaternions have exactly the same elements in the same position (when compared with ==)
    *
    * @param {Quat2} a the first dual quaternion.
    * @param {Quat2} b the second dual quaternion.
    * @returns {Boolean} true if the dual quaternions are equal, false otherwise.
    */
    public static function exactEquals( a : Quat2, b : Quat2 ) : Bool {
    return a[0] == b[0] && a[1] == b[1] && a[2] == b[2] && a[3] == b[3] &&
        a[4] == b[4] && a[5] == b[5] && a[6] == b[6] && a[7] == b[7];
    }

    /**
    * Returns whether or not the dual quaternions have approximately the same elements in the same position.
    *
    * @param {Quat2} a the first dual quat.
    * @param {Quat2} b the second dual quat.
    * @returns {Boolean} true if the dual quats are equal, false otherwise.
    */
    public static function equals( a : Quat2, b : Quat2 ) : Bool {
    var a0 = a[0],
        a1 = a[1],
        a2 = a[2],
        a3 = a[3],
        a4 = a[4],
        a5 = a[5],
        a6 = a[6],
        a7 = a[7];
    var b0 = b[0],
        b1 = b[1],
        b2 = b[2],
        b3 = b[3],
        b4 = b[4],
        b5 = b[5],
        b6 = b[6],
        b7 = b[7];
    return (Math.abs(a0 - b0) <= GLMatrix.EPSILON * Math.max(1.0, Math.max(Math.abs(a0), Math.abs(b0))) &&
        Math.abs(a1 - b1) <= GLMatrix.EPSILON * Math.max(1.0, Math.max(Math.abs(a1), Math.abs(b1))) &&
        Math.abs(a2 - b2) <= GLMatrix.EPSILON * Math.max(1.0, Math.max(Math.abs(a2), Math.abs(b2))) &&
        Math.abs(a3 - b3) <= GLMatrix.EPSILON * Math.max(1.0, Math.max(Math.abs(a3), Math.abs(b3))) &&
        Math.abs(a4 - b4) <= GLMatrix.EPSILON * Math.max(1.0, Math.max(Math.abs(a4), Math.abs(b4))) &&
        Math.abs(a5 - b5) <= GLMatrix.EPSILON * Math.max(1.0, Math.max(Math.abs(a5), Math.abs(b5))) &&
        Math.abs(a6 - b6) <= GLMatrix.EPSILON * Math.max(1.0, Math.max(Math.abs(a6), Math.abs(b6))) &&
        Math.abs(a7 - b7) <= GLMatrix.EPSILON * Math.max(1.0, Math.max(Math.abs(a7), Math.abs(b7))));
    }
}
