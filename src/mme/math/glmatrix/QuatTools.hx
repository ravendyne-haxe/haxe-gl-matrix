package mme.math.glmatrix;

import mme.math.glmatrix.GLMatrix;

import mme.math.glmatrix.Quat;
import mme.math.glmatrix.Vec3;
import mme.math.glmatrix.Mat3;

import mme.math.glmatrix.Vec3Tools;
import mme.math.glmatrix.Mat3Tools;

/**
 * Quaternion
 * @module Quat
 */
class QuatTools {

/*

//
// GENERATORS in Quat class
//

Quat.fromValues( ?out : Quat, x : Float, y : Float, z : Float, w : Float ) : Quat;
Quat.fromAxisAngle( ?out : Quat, axis : Vec3, rad : Float ) : Quat;
Quat.fromAxes( ?out : Quat, view : Vec3, right : Vec3, up : Vec3 ) : Quat;
Quat.fromMat3( ?out : Quat, m : Mat3 ) : Quat;
Quat.fromEuler( ?out : Quat, x : Float, y : Float, z : Float ) : Quat;


//
// GENERATORS
//
create() : Quat;
quat.clone( a : Quat ) : Quat;
?quat.identity( ?out : Quat ) : Quat;
?quat.random( ?out : Quat ) : Quat;

?quat.rotationTo( ?out : Quat, a : Vec3, b : Vec3 ) : Quat;

slerp( t : Float, a : Quat, b : Quat, ?out : Quat ) : Quat;
lerp( t : Float, a : Quat, b : Quat, ?out : Quat ) : Quat;
sqlerp( t : Float, a : Quat, b : Quat, c : Quat, d : Quat, ?out : Quat ) : Quat;


//
// EDIT
//
quat.set( out : Quat, x : Float, y : Float, z : Float, w : Float ) : Quat;


//
// OPERATORS
//

// unary
//
quat.invert( a : Quat, ?out : Quat ) : Quat;
quat.conjugate( a : Quat, ?out : Quat ) : Quat;
quat.copy( a : Quat, ?out : Quat ) : Quat;
quat.normalize( a : Quat, ?out : Quat ) : Quat;
quat.calculateW( a : Quat, ?out : Quat ) : Quat;


// unary, non-Quat result
//
quat.length( a : Quat ) : Float;
quat.squaredLength( a : Quat ) : Float;
quat.str( a : Quat ) : String;


// binary
//
quat.multiply( a : Quat, b : Quat, ?out : Quat ) : Quat;
quat.add( a : Quat, b : Quat, ?out : Quat ) : Quat;


// binary, non-Quat arg
//
quat.multiplyVec3( a : Quat, v : Vec3, ?out : Vec3 ) : Vec3;
quat.multiplyVec4( a : Quat, v : Vec4, ?out : Vec4 ) : Vec4;
quat.scale( a : Quat, b : Float, ?out : Quat ) : Quat;
quat.rotateX( a : Quat, rad : Float, ?out : Quat ) : Quat;
quat.rotateY( a : Quat, rad : Float, ?out : Quat ) : Quat;
quat.rotateZ( a : Quat, rad : Float, ?out : Quat ) : Quat;


// binary, non-Quat result
//
quat.dot( a : Quat, b : Quat ) : Float;
quat.exactEquals( a : Quat, b : Quat ) : Bool;
quat.equals( a : Quat, b : Quat ) : Bool;


// MISC
//
quat.getAxisAngle( a : Quat, out_axis : Vec3 ) : Float;

*/
    /**
    * Creates a new identity Quat
    *
    * @returns {Quat} a new quaternion
    */
    public static inline function create() : Quat {
    var out = new Quat();
    return out;
    }

    /**
    * Set a Quat to the identity quaternion
    *
    * @param {Quat} out the receiving quaternion
    * @returns {Quat} out
    */
    public static function identity( ?out : Quat ) : Quat {
    if( out == null ) out = create();
    out[0] = 0;
    out[1] = 0;
    out[2] = 0;
    out[3] = 1;
    return out;
    }

    /**
    * Gets the rotation axis and angle for a given
    *  quaternion. If a quaternion is created with
    *  fromAxisAngle, this method will return the same
    *  values as providied in the original parameter list
    *  OR functionally equivalent values.
    * Example: The quaternion formed by axis [0, 0, 1] and
    *  angle -90 is the same as the quaternion formed by
    *  [0, 0, 1] and 270. This method favors the latter.
    * @param  {Vec3} out_axis  Vector receiving the axis of rotation
    * @param  {Quat} a     Quaternion to be decomposed
    * @return {Float}     Angle, in radians, of the rotation
    */
    public static function getAxisAngle( a : Quat, out_axis : Vec3 ) : Float {
    var rad = Math.acos(a[3]) * 2.0;
    var s = Math.sin(rad / 2.0);
    if (s > GLMatrix.EPSILON) {
        out_axis[0] = a[0] / s;
        out_axis[1] = a[1] / s;
        out_axis[2] = a[2] / s;
    } else {
        // If s is zero, return any axis (no rotation - axis does not matter)
        out_axis[0] = 1;
        out_axis[1] = 0;
        out_axis[2] = 0;
    }
    return rad;
    }

    /**
    * Multiplies two Quat's
    *
    * @param {Quat} out the receiving quaternion
    * @param {Quat} a the first operand
    * @param {Quat} b the second operand
    * @returns {Quat} out
    */
    public static function multiply( a : Quat, b : Quat, ?out : Quat ) : Quat {
    if( out == null ) out = create();
    var ax = a[0], ay = a[1], az = a[2], aw = a[3];
    var bx = b[0], by = b[1], bz = b[2], bw = b[3];

    out[0] = ax * bw + aw * bx + ay * bz - az * by;
    out[1] = ay * bw + aw * by + az * bx - ax * bz;
    out[2] = az * bw + aw * bz + ax * by - ay * bx;
    out[3] = aw * bw - ax * bx - ay * by - az * bz;
    return out;
    }

    /**
    * Rotates a quaternion by the given angle about the X axis
    *
    * @param {Quat} out Quat receiving operation result
    * @param {Quat} a Quat to rotate
    * @param {Float} rad angle (in radians) to rotate
    * @returns {Quat} out
    */
    public static function rotateX( a : Quat, rad : Float, ?out : Quat ) : Quat {
    if( out == null ) out = create();
    rad *= 0.5;

    var ax = a[0], ay = a[1], az = a[2], aw = a[3];
    var bx = Math.sin(rad), bw = Math.cos(rad);

    out[0] = ax * bw + aw * bx;
    out[1] = ay * bw + az * bx;
    out[2] = az * bw - ay * bx;
    out[3] = aw * bw - ax * bx;
    return out;
    }

    /**
    * Rotates a quaternion by the given angle about the Y axis
    *
    * @param {Quat} out Quat receiving operation result
    * @param {Quat} a Quat to rotate
    * @param {Float} rad angle (in radians) to rotate
    * @returns {Quat} out
    */
    public static function rotateY( a : Quat, rad : Float, ?out : Quat ) : Quat {
    if( out == null ) out = create();
    rad *= 0.5;

    var ax = a[0], ay = a[1], az = a[2], aw = a[3];
    var by = Math.sin(rad), bw = Math.cos(rad);

    out[0] = ax * bw - az * by;
    out[1] = ay * bw + aw * by;
    out[2] = az * bw + ax * by;
    out[3] = aw * bw - ay * by;
    return out;
    }

    /**
    * Rotates a quaternion by the given angle about the Z axis
    *
    * @param {Quat} out Quat receiving operation result
    * @param {Quat} a Quat to rotate
    * @param {Float} rad angle (in radians) to rotate
    * @returns {Quat} out
    */
    public static function rotateZ( a : Quat, rad : Float, ?out : Quat ) : Quat {
    if( out == null ) out = create();
    rad *= 0.5;

    var ax = a[0], ay = a[1], az = a[2], aw = a[3];
    var bz = Math.sin(rad), bw = Math.cos(rad);

    out[0] = ax * bw + ay * bz;
    out[1] = ay * bw - ax * bz;
    out[2] = az * bw + aw * bz;
    out[3] = aw * bw - az * bz;
    return out;
    }

    /**
    * Calculates the W component of a Quat from the X, Y, and Z components.
    * Assumes that quaternion is 1 unit in length.
    * Any existing W component will be ignored.
    *
    * @param {Quat} out the receiving quaternion
    * @param {Quat} a Quat to calculate W component of
    * @returns {Quat} out
    */
    public static function calculateW( a : Quat, ?out : Quat ) : Quat {
    if( out == null ) out = create();
    var x = a[0], y = a[1], z = a[2];

    out[0] = x;
    out[1] = y;
    out[2] = z;
    out[3] = Math.sqrt(Math.abs(1.0 - x * x - y * y - z * z));
    return out;
    }

    /**
    * Performs a spherical linear interpolation between two Quat
    *
    * @param {Quat} out the receiving quaternion
    * @param {Quat} a the first operand
    * @param {Quat} b the second operand
    * @param {Float} t interpolation amount, in the range [0-1], between the two inputs
    * @returns {Quat} out
    */
    public static function slerp( t : Float, a : Quat, b : Quat, ?out : Quat ) : Quat {
    if( out == null ) out = create();
    // benchmarks:
    //    http://jsperf.com/quaternion-slerp-implementations
    var ax = a[0], ay = a[1], az = a[2], aw = a[3];
    var bx = b[0], by = b[1], bz = b[2], bw = b[3];

    var omega, cosom, sinom, scale0, scale1;

    // calc cosine
    cosom = ax * bx + ay * by + az * bz + aw * bw;
    // adjust signs (if necessary)
    if ( cosom < 0.0 ) {
        cosom = -cosom;
        bx = - bx;
        by = - by;
        bz = - bz;
        bw = - bw;
    }
    // calculate coefficients
    if ( (1.0 - cosom) > GLMatrix.EPSILON ) {
        // standard case (slerp)
        omega  = Math.acos(cosom);
        sinom  = Math.sin(omega);
        scale0 = Math.sin((1.0 - t) * omega) / sinom;
        scale1 = Math.sin(t * omega) / sinom;
    } else {
        // "from" and "to" quaternions are very close
        //  ... so we can do a linear interpolation
        scale0 = 1.0 - t;
        scale1 = t;
    }
    // calculate final values
    out[0] = scale0 * ax + scale1 * bx;
    out[1] = scale0 * ay + scale1 * by;
    out[2] = scale0 * az + scale1 * bz;
    out[3] = scale0 * aw + scale1 * bw;

    return out;
    }

    /**
    * Generates a random quaternion
    *
    * @param {Quat} out the receiving quaternion
    * @returns {Quat} out
    */
    public static function random( ?out : Quat ) : Quat {
    if( out == null ) out = create();
    // Implementation of http://planning.cs.uiuc.edu/node198.html
    // TODO: Calling random 3 times is probably not the fastest solution
    var u1 = GLMatrix.RANDOM();
    var u2 = GLMatrix.RANDOM();
    var u3 = GLMatrix.RANDOM();

    var sqrt1MinusU1 = Math.sqrt(1 - u1);
    var sqrtU1 = Math.sqrt(u1);

    out[0] = sqrt1MinusU1 * Math.sin(2.0 * Math.PI * u2);
    out[1] = sqrt1MinusU1 * Math.cos(2.0 * Math.PI * u2);
    out[2] = sqrtU1 * Math.sin(2.0 * Math.PI * u3);
    out[3] = sqrtU1 * Math.cos(2.0 * Math.PI * u3);
    return out;
    }

    /**
    * Calculates the inverse of a Quat
    *
    * @param {Quat} out the receiving quaternion
    * @param {Quat} a Quat to calculate inverse of
    * @returns {Quat} out
    */
    public static function invert( a : Quat, ?out : Quat ) : Quat {
    if( out == null ) out = create();
    var a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3];
    var dot = a0*a0 + a1*a1 + a2*a2 + a3*a3;
    var invDot = dot != 0.0 ? 1.0/dot : 0.0;

    // TODO: Would be faster to return [0,0,0,0] immediately if dot == 0

    out[0] = -a0*invDot;
    out[1] = -a1*invDot;
    out[2] = -a2*invDot;
    out[3] = a3*invDot;
    return out;
    }

    /**
    * Calculates the conjugate of a Quat
    * If the quaternion is normalized, this function is faster than Quat.inverse and produces the same result.
    *
    * @param {Quat} out the receiving quaternion
    * @param {Quat} a Quat to calculate conjugate of
    * @returns {Quat} out
    */
    public static function conjugate( a : Quat, ?out : Quat ) : Quat {
    if( out == null ) out = create();
    out[0] = -a[0];
    out[1] = -a[1];
    out[2] = -a[2];
    out[3] = a[3];
    return out;
    }

    /**
    * Returns a string representation of a quatenion
    *
    * @param {Quat} a vector to represent as a string
    * @returns {String} string representation of the vector
    */
    public static function str( a : Quat ) : String {
    return 'Quat(' + a[0] + ', ' + a[1] + ', ' + a[2] + ', ' + a[3] + ')';
    }

    /**
    * Creates a new Quat initialized with values from an existing quaternion
    *
    * @param {Quat} a quaternion to clone
    * @returns {Quat} a new quaternion
    * @function
    */
    // public static var clone = Vec4Tools.clone;
    public static function clone( a : Quat ) : Quat {
    var out = new Quat();
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
    return out;
    }

    /**
    * Copy the values from one Quat to another
    *
    * @param {Quat} out the receiving quaternion
    * @param {Quat} a the source quaternion
    * @returns {Quat} out
    * @function
    */
    // public static var copy = Vec4Tools.copy;
    public static function copy( a : Quat, ?out : Quat ) : Quat {
    if( out == null ) out = create();
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
    return out;
    }

    /**
    * Set the components of a Quat to the given values
    *
    * @param {Quat} out the receiving quaternion
    * @param {Float} x X component
    * @param {Float} y Y component
    * @param {Float} z Z component
    * @param {Float} w W component
    * @returns {Quat} out
    * @function
    */
    // public static var set = Vec4Tools.set;
    public static function set( out : Quat, x : Float, y : Float, z : Float, w : Float ) : Quat {
    out[0] = x;
    out[1] = y;
    out[2] = z;
    out[3] = w;
    return out;
    }

    /**
    * Adds two Quat's
    *
    * @param {Quat} out the receiving quaternion
    * @param {Quat} a the first operand
    * @param {Quat} b the second operand
    * @returns {Quat} out
    * @function
    */
    // public static var add = Vec4Tools.add;
    public static function add( a : Quat, b : Quat, ?out : Quat ) : Quat {
    if( out == null ) out = create();
    out[0] = a[0] + b[0];
    out[1] = a[1] + b[1];
    out[2] = a[2] + b[2];
    out[3] = a[3] + b[3];
    return out;
    }

    /**
    * Alias for {@link Quat.multiply}
    * @function
    */
    public static var mul = multiply;

    /**
    * Multiply Quat with Vec3
    * Can also be used for dual quaternions. (Multiply it with the real part)
    * out = a * v
    *
    * @param {Vec3} out the receiving vector
    * @param {Quat} a quaternion to be multiplied by vector
    * @param {Vec3} v the vector to multiply quaternion with
    * @returns {Vec3} out
    */
    public static function multiplyVec3( a : Quat, v : Vec3, ?out : Vec3 ) : Vec3 {
    if( out == null ) out = Vec3Tools.create();
        // benchmarks: https://jsperf.com/quaternion-transform-vec4-implementations-fixed
        var qx = a[0], qy = a[1], qz = a[2], qw = a[3];
        var x = v[0], y = v[1], z = v[2];
        // var qvec = [qx, qy, qz];
        // var uv = Vec3.cross([], qvec, v);
        var uvx = qy * z - qz * y,
            uvy = qz * x - qx * z,
            uvz = qx * y - qy * x;
        // var uuv = Vec3.cross([], qvec, uv);
        var uuvx = qy * uvz - qz * uvy,
            uuvy = qz * uvx - qx * uvz,
            uuvz = qx * uvy - qy * uvx;
        // Vec3.scale(uv, uv, 2 * w);
        var w2 = qw * 2;
        uvx *= w2;
        uvy *= w2;
        uvz *= w2;
        // Vec3.scale(uuv, uuv, 2);
        uuvx *= 2;
        uuvy *= 2;
        uuvz *= 2;
        // return Vec3.add(out, v, Vec3.add(out, uv, uuv));
        out[0] = x + uvx + uuvx;
        out[1] = y + uvy + uuvy;
        out[2] = z + uvz + uuvz;
        return out;
    }

    /**
    * Multiply Quat with Vec4
    * out = a * v
    *
    * @param {Vec4} out the receiving vector
    * @param {Quat} a quaternion to be multiplied by vector
    * @param {Vec4} v the vector to multiply quaternion with
    * @returns {Vec4} out
    */
    public static function multiplyVec4( a : Quat, v : Vec4, ?out : Vec4 ) : Vec4 {
    if( out == null ) out = Vec4Tools.create();
    var x = v[0], y = v[1], z = v[2];
    var qx = a[0], qy = a[1], qz = a[2], qw = a[3];

    // calculate quat * vec
    var ix = qw * x + qy * z - qz * y;
    var iy = qw * y + qz * x - qx * z;
    var iz = qw * z + qx * y - qy * x;
    var iw = -qx * x - qy * y - qz * z;

    // calculate result * inverse quat
    out[0] = ix * qw + iw * -qx + iy * -qz - iz * -qy;
    out[1] = iy * qw + iw * -qy + iz * -qx - ix * -qz;
    out[2] = iz * qw + iw * -qz + ix * -qy - iy * -qx;
    out[3] = v[3];
    return out;
    }

    /**
    * Scales a Quat by a scalar Float
    *
    * @param {Quat} out the receiving vector
    * @param {Quat} a the vector to scale
    * @param {Float} b amount to scale the vector by
    * @returns {Quat} out
    * @function
    */
    // public static var scale = Vec4Tools.scale;
    public static function scale( a : Quat, b : Float, ?out : Quat ) : Quat {
    if( out == null ) out = create();
    out[0] = a[0] * b;
    out[1] = a[1] * b;
    out[2] = a[2] * b;
    out[3] = a[3] * b;
    return out;
    }

    /**
    * Calculates the dot product of two Quat's
    *
    * @param {Quat} a the first operand
    * @param {Quat} b the second operand
    * @returns {Float} dot product of a and b
    * @function
    */
    // public static var dot = Vec4Tools.dot;
    public static function dot( a : Quat, b : Quat ) : Float {
    return a[0] * b[0] + a[1] * b[1] + a[2] * b[2] + a[3] * b[3];
    }

    /**
    * Performs a linear interpolation between two Quat's
    *
    * @param {Quat} out the receiving quaternion
    * @param {Quat} a the first operand
    * @param {Quat} b the second operand
    * @param {Float} t interpolation amount, in the range [0-1], between the two inputs
    * @returns {Quat} out
    * @function
    */
    // public static var lerp = Vec4Tools.lerp;
    public static function lerp( t : Float, a : Quat, b : Quat, ?out : Quat ) : Quat {
    if( out == null ) out = create();
    var ax = a[0];
    var ay = a[1];
    var az = a[2];
    var aw = a[3];
    out[0] = ax + t * (b[0] - ax);
    out[1] = ay + t * (b[1] - ay);
    out[2] = az + t * (b[2] - az);
    out[3] = aw + t * (b[3] - aw);
    return out;
    }

    /**
    * Calculates the length of a Quat
    *
    * @param {Quat} a vector to calculate length of
    * @returns {Float} length of a
    */
    // public static var length = Vec4Tools.length;
    public static function length( a : Quat ) : Float {
    var x = a[0];
    var y = a[1];
    var z = a[2];
    var w = a[3];
    return Math.sqrt(x*x + y*y + z*z + w*w);
    }

    /**
    * Alias for {@link Quat.length}
    * @function
    */
    public static var len = length;

    /**
    * Calculates the squared length of a Quat
    *
    * @param {Quat} a vector to calculate squared length of
    * @returns {Float} squared length of a
    * @function
    */
    // public static var squaredLength = Vec4Tools.squaredLength;
    public static function squaredLength( a : Quat ) : Float {
    var x = a[0];
    var y = a[1];
    var z = a[2];
    var w = a[3];
    return x*x + y*y + z*z + w*w;
    }

    /**
    * Alias for {@link Quat.squaredLength}
    * @function
    */
    public static var sqrLen = squaredLength;

    /**
    * Normalize a Quat
    *
    * @param {Quat} out the receiving quaternion
    * @param {Quat} a quaternion to normalize
    * @returns {Quat} out
    * @function
    */
    // public static var normalize = Vec4Tools.normalize;
    public static function normalize( a : Quat, ?out : Quat ) : Quat {
    if( out == null ) out = create();
    var x = a[0];
    var y = a[1];
    var z = a[2];
    var w = a[3];
    var len = x*x + y*y + z*z + w*w;
    if (len > 0) {
        len = 1 / Math.sqrt(len);
    }
    out[0] = x * len;
    out[1] = y * len;
    out[2] = z * len;
    out[3] = w * len;
    return out;
    }

    /**
    * Returns whether or not the quaternions have exactly the same elements in the same position (when compared with ==)
    *
    * @param {Quat} a The first quaternion.
    * @param {Quat} b The second quaternion.
    * @returns {Boolean} True if the vectors are equal, false otherwise.
    */
    // public static var exactEquals = Vec4Tools.exactEquals;
    public static function exactEquals( a : Quat, b : Quat ) : Bool {
    return a[0] == b[0] && a[1] == b[1] && a[2] == b[2] && a[3] == b[3];
    }

    /**
    * Returns whether or not the quaternions have approximately the same elements in the same position.
    *
    * @param {Quat} a The first vector.
    * @param {Quat} b The second vector.
    * @returns {Boolean} True if the vectors are equal, false otherwise.
    */
    // public static var equals = Vec4Tools.equals;
    public static function equals( a : Quat, b : Quat ) : Bool {
    var a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3];
    var b0 = b[0], b1 = b[1], b2 = b[2], b3 = b[3];
    return (Math.abs(a0 - b0) <= GLMatrix.EPSILON*Math.max(1.0, Math.max(Math.abs(a0), Math.abs(b0))) &&
            Math.abs(a1 - b1) <= GLMatrix.EPSILON*Math.max(1.0, Math.max(Math.abs(a1), Math.abs(b1))) &&
            Math.abs(a2 - b2) <= GLMatrix.EPSILON*Math.max(1.0, Math.max(Math.abs(a2), Math.abs(b2))) &&
            Math.abs(a3 - b3) <= GLMatrix.EPSILON*Math.max(1.0, Math.max(Math.abs(a3), Math.abs(b3))));
    }

    /**
    * Sets a quaternion to represent the shortest rotation from one
    * vector to another.
    *
    * Both vectors are assumed to be unit length.
    *
    * @param {Quat} out the receiving quaternion.
    * @param {Vec3} a the initial vector
    * @param {Vec3} b the destination vector
    * @returns {Quat} out
    */
    public static function rotationTo( ?out : Quat, a : Vec3, b : Vec3 ) : Quat {
    var tmpvec3 = Vec3Tools.create();
    var xUnitVec3 = Vec3.fromValues(1,0,0);
    var yUnitVec3 = Vec3.fromValues(0,1,0);

    // return function(out, a, b) {
        if( out == null ) out = create();
        var dot = Vec3Tools.dot(a, b);
        if (dot < -0.999999) {
        Vec3Tools.cross(tmpvec3, xUnitVec3, a);
        if (Vec3Tools.len(tmpvec3) < 0.000001)
            Vec3Tools.cross(tmpvec3, yUnitVec3, a);
        Vec3Tools.normalize(tmpvec3, tmpvec3);
        Quat.fromAxisAngle(out, tmpvec3, Math.PI);
        return out;
        } else if (dot > 0.999999) {
        out[0] = 0;
        out[1] = 0;
        out[2] = 0;
        out[3] = 1;
        return out;
        } else {
        Vec3Tools.cross(tmpvec3, a, b);
        out[0] = tmpvec3[0];
        out[1] = tmpvec3[1];
        out[2] = tmpvec3[2];
        out[3] = 1 + dot;
        return normalize(out, out);
        }
    };
    // })();

    /**
    * Performs a spherical linear interpolation with two control points
    *
    * @param {Quat} out the receiving quaternion
    * @param {Quat} a the first operand
    * @param {Quat} b the second operand
    * @param {Quat} c the third operand
    * @param {Quat} d the fourth operand
    * @param {Float} t interpolation amount, in the range [0-1], between the two inputs
    * @returns {Quat} out
    */
    public static function sqlerp( t : Float, a : Quat, b : Quat, c : Quat, d : Quat, ?out : Quat ) : Quat {
    var temp1 = create();
    var temp2 = create();

    // return function (out, a, b, c, d, t) {
        if( out == null ) out = create();
        slerp(t, a, d, temp1);
        slerp(t, b, c, temp2);
        slerp(2 * t * (1 - t), temp1, temp2, out);

        return out;
    };
    // }());
}
