package mme.math.glmatrix;

class GLMatrix {

    public static inline var EPSILON = 0.000001;

    // public static inline function RANDOM() { return Math.random(); }
    public static var RANDOM ( default, null ) = Math.random;

    public static var degree ( default, null ) = Math.PI / 180.0;

    /**
    * Convert Degree To Radian
    *
    * @param {Float} a Angle in Degrees
    */
    public static inline function toRadian( a : Float ) {
        return a * degree;
    }

    /**
    * Tests whether or not the arguments have approximately the same value, within an absolute
    * or relative tolerance of GLMatrix.EPSILON (an absolute tolerance is used for values less
    * than or equal to 1.0, and a relative tolerance is used for larger values)
    *
    * @param {Float} a The first number to test.
    * @param {Float} b The second number to test.
    * @returns {Bool} True if the numbers are approximately equal, false otherwise.
    */
    public static inline function equals( a : Float, b : Float ) {

        return Math.abs( a - b ) <= EPSILON * Math.max( 1.0, Math.max( Math.abs( a ), Math.abs( b ) ) );
    }
}
