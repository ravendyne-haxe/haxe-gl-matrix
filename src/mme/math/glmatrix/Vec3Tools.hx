package mme.math.glmatrix;

import mme.math.glmatrix.GLMatrix;

import mme.math.glmatrix.Vec3;
import mme.math.glmatrix.Mat3;
import mme.math.glmatrix.Mat4;
import mme.math.glmatrix.Quat;

/**
 * 3 Dimensional Vector
 * @module Vec3
 */
class Vec3Tools {

/*

//
// GENERATORS in Vec3 class
//

Vec3.fromValues( ?out : Vec3, x : Float, y : Float, z : Float ) : Vec3;


//
// GENERATORS
//
create() : Vec3;
vec3.clone( a : Vec3 ) : Vec3;

?vec3.random( ?out : Vec3, scale : Float = 1.0 ) : Vec3;

lerp( t : Float, a : Vec3, b : Vec3, ?out : Vec3 ) : Vec3;
hermite( t : Float, a : Vec3, b : Vec3, c : Vec3, d : Vec3, ?out : Vec3 ) : Vec3;
bezier( t : Float, a : Vec3, b : Vec3, c : Vec3, d : Vec3, ?out : Vec3 ) : Vec3;


//
// EDIT
//
vec3.set( out : Vec3, x : Float, y : Float, z : Float ) : Vec3;
vec3.zero( out : Vec3 ) : Vec3;


//
// OPERATORS
//

// unary
//
vec3.copy( a : Vec3, ?out : Vec3 ) : Vec3;
vec3.ceil( a : Vec3, ?out : Vec3 ) : Vec3;
vec3.floor( a : Vec3, ?out : Vec3 ) : Vec3;
vec3.round( a : Vec3, ?out : Vec3 ) : Vec3;
vec3.negate( a : Vec3, ?out : Vec3 ) : Vec3;
vec3.inverse( a : Vec3, ?out : Vec3 ) : Vec3;
vec3.normalize( a : Vec3, ?out : Vec3 ) : Vec3;

// unary, non-Vec3 result
//
vec3.length( a : Vec3 ) : Float;
vec3.squaredLength( a : Vec3 ) : Float;
vec3.str( a : Vec3 ) : String;


// binary
//
vec3.add( a : Vec3, b : Vec3, ?out : Vec3 ) : Vec3;
vec3.subtract( a : Vec3, b : Vec3, ?out : Vec3 ) : Vec3;
vec3.multiply( a : Vec3, b : Vec3, ?out : Vec3 ) : Vec3;
vec3.divide( a : Vec3, b : Vec3, ?out : Vec3 ) : Vec3;
vec3.min( a : Vec3, b : Vec3, ?out : Vec3 ) : Vec3;
vec3.max( a : Vec3, b : Vec3, ?out : Vec3 ) : Vec3;
vec3.cross( a : Vec3, b : Vec3, ?out : Vec3 ) : Vec3;

// binary, non-Vec3 arg
//
vec3.scale( a : Vec3, b : Float, ?out : Vec3 ) : Vec3;
vec3.postmultiplyMat4( a : Vec3, m : Mat4, ?out : Vec3 ) : Vec3; // a.k.a. Mat4Tools.multiplyVec3()
vec3.postmultiplyMat3( a : Vec3, m : Mat3, ?out : Vec3 ) : Vec3; // a.k.a. Mat3Tools.multiplyVec3()
vec3.postmultiplyQuat( a : Vec3, q : Quat, ?out : Vec3 ) : Vec3; // a.k.a. QuatTools.multiplyVec3()

// binary, non-Vec3 result
//
vec3.dot( a : Vec3, b : Vec3 ) : Float;
vec3.angle( a : Vec3, b : Vec3 ) : Float;
vec3.exactEquals( a : Vec3, b : Vec3 ) : Bool;
vec3.equals( a : Vec3, b : Vec3 ) : Bool;
vec3.squaredDistance( a : Vec3, b : Vec3 ) : Float;
vec3.distance( a : Vec3, b : Vec3 ) : Float;


// ternary
//
vec3.scaleAndAdd( a : Vec3, b : Vec3, scale : Float, ?out : Vec3 ) : Vec3;
vec3.rotateX( a : Vec3, b : Vec3, c : Float, ?out : Vec3 ) : Vec3;
vec3.rotateY( a : Vec3, b : Vec3, c : Float, ?out : Vec3 ) : Vec3;
vec3.rotateZ( a : Vec3, b : Vec3, c : Float, ?out : Vec3 ) : Vec3;

*/

    /**
    * Creates a new, empty Vec3
    *
    * @returns {Vec3} a new 3D vector
    */
    public static inline function create() : Vec3 {
        var out = new Vec3();
        return out;
    }

    /**
    * Creates a new Vec3 initialized with values from an existing vector
    *
    * @param {Vec3} a vector to clone
    * @returns {Vec3} a new 3D vector
    */
    public static function clone( a : Vec3 ) : Vec3 {
        var out = new Vec3();
        out[0] = a[0];
        out[1] = a[1];
        out[2] = a[2];
        return out;
    }

    /**
    * Calculates the length of a Vec3
    *
    * @param {Vec3} a vector to calculate length of
    * @returns {Float} length of a
    */
    public static function length( a : Vec3 ) : Float {
    var x = a[0];
    var y = a[1];
    var z = a[2];
    return Math.sqrt(x*x + y*y + z*z);
    }

    /**
    * Copy the values from one Vec3 to another
    *
    * @param {Vec3} out the receiving vector
    * @param {Vec3} a the source vector
    * @returns {Vec3} out
    */
    public static function copy( a : Vec3, ?out : Vec3 ) : Vec3 {
    if( out == null ) out = create();
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    return out;
    }

    /**
    * Set the components of a Vec3 to the given values
    *
    * @param {Vec3} out the receiving vector
    * @param {Float} x X component
    * @param {Float} y Y component
    * @param {Float} z Z component
    * @returns {Vec3} out
    */
    public static function set( out : Vec3, x : Float, y : Float, z : Float ) : Vec3 {
    out[0] = x;
    out[1] = y;
    out[2] = z;
    return out;
    }

    /**
    * Adds two Vec3's
    *
    * @param {Vec3} out the receiving vector
    * @param {Vec3} a the first operand
    * @param {Vec3} b the second operand
    * @returns {Vec3} out
    */
    public static function add( a : Vec3, b : Vec3, ?out : Vec3 ) : Vec3 {
    if( out == null ) out = create();
    out[0] = a[0] + b[0];
    out[1] = a[1] + b[1];
    out[2] = a[2] + b[2];
    return out;
    }

    /**
    * Subtracts vector b from vector a
    *
    * @param {Vec3} out the receiving vector
    * @param {Vec3} a the first operand
    * @param {Vec3} b the second operand
    * @returns {Vec3} out
    */
    public static function subtract( a : Vec3, b : Vec3, ?out : Vec3 ) : Vec3 {
    if( out == null ) out = create();
    out[0] = a[0] - b[0];
    out[1] = a[1] - b[1];
    out[2] = a[2] - b[2];
    return out;
    }

    /**
    * Multiplies two Vec3's
    *
    * @param {Vec3} out the receiving vector
    * @param {Vec3} a the first operand
    * @param {Vec3} b the second operand
    * @returns {Vec3} out
    */
    public static function multiply( a : Vec3, b : Vec3, ?out : Vec3 ) : Vec3 {
    if( out == null ) out = create();
    out[0] = a[0] * b[0];
    out[1] = a[1] * b[1];
    out[2] = a[2] * b[2];
    return out;
    }

    /**
    * Divides two Vec3's
    *
    * @param {Vec3} out the receiving vector
    * @param {Vec3} a the first operand
    * @param {Vec3} b the second operand
    * @returns {Vec3} out
    */
    public static function divide( a : Vec3, b : Vec3, ?out : Vec3 ) : Vec3 {
    if( out == null ) out = create();
    out[0] = a[0] / b[0];
    out[1] = a[1] / b[1];
    out[2] = a[2] / b[2];
    return out;
    }

    /**
    * Math.ceil the components of a Vec3
    *
    * @param {Vec3} out the receiving vector
    * @param {Vec3} a vector to ceil
    * @returns {Vec3} out
    */
    public static function ceil( a : Vec3, ?out : Vec3 ) : Vec3 {
    if( out == null ) out = create();
    out[0] = Math.ceil(a[0]);
    out[1] = Math.ceil(a[1]);
    out[2] = Math.ceil(a[2]);
    return out;
    }

    /**
    * Math.floor the components of a Vec3
    *
    * @param {Vec3} out the receiving vector
    * @param {Vec3} a vector to floor
    * @returns {Vec3} out
    */
    public static function floor( a : Vec3, ?out : Vec3 ) : Vec3 {
    if( out == null ) out = create();
    out[0] = Math.floor(a[0]);
    out[1] = Math.floor(a[1]);
    out[2] = Math.floor(a[2]);
    return out;
    }

    /**
    * Returns the minimum of two Vec3's
    *
    * @param {Vec3} out the receiving vector
    * @param {Vec3} a the first operand
    * @param {Vec3} b the second operand
    * @returns {Vec3} out
    */
    public static function min( a : Vec3, b : Vec3, ?out : Vec3 ) : Vec3 {
    if( out == null ) out = create();
    out[0] = Math.min(a[0], b[0]);
    out[1] = Math.min(a[1], b[1]);
    out[2] = Math.min(a[2], b[2]);
    return out;
    }

    /**
    * Returns the maximum of two Vec3's
    *
    * @param {Vec3} out the receiving vector
    * @param {Vec3} a the first operand
    * @param {Vec3} b the second operand
    * @returns {Vec3} out
    */
    public static function max( a : Vec3, b : Vec3, ?out : Vec3 ) : Vec3 {
    if( out == null ) out = create();
    out[0] = Math.max(a[0], b[0]);
    out[1] = Math.max(a[1], b[1]);
    out[2] = Math.max(a[2], b[2]);
    return out;
    }

    /**
    * Math.round the components of a Vec3
    *
    * @param {Vec3} out the receiving vector
    * @param {Vec3} a vector to round
    * @returns {Vec3} out
    */
    public static function round( a : Vec3, ?out : Vec3 ) : Vec3 {
    if( out == null ) out = create();
    out[0] = Math.round(a[0]);
    out[1] = Math.round(a[1]);
    out[2] = Math.round(a[2]);
    return out;
    }

    /**
    * Scales a Vec3 by a scalar number
    *
    * @param {Vec3} out the receiving vector
    * @param {Vec3} a the vector to scale
    * @param {Float} b amount to scale the vector by
    * @returns {Vec3} out
    */
    public static function scale( a : Vec3, b : Float, ?out : Vec3 ) : Vec3 {
    if( out == null ) out = create();
    out[0] = a[0] * b;
    out[1] = a[1] * b;
    out[2] = a[2] * b;
    return out;
    }

    /**
    * Adds two Vec3's after scaling the second operand by a scalar value
    *
    * @param {Vec3} out the receiving vector
    * @param {Vec3} a the first operand
    * @param {Vec3} b the second operand
    * @param {Float} scale the amount to scale b by before adding
    * @returns {Vec3} out
    */
    public static function scaleAndAdd( a : Vec3, b : Vec3, scale : Float, ?out : Vec3 ) : Vec3 {
    if( out == null ) out = create();
    out[0] = a[0] + (b[0] * scale);
    out[1] = a[1] + (b[1] * scale);
    out[2] = a[2] + (b[2] * scale);
    return out;
    }

    /**
    * Calculates the euclidian distance between two Vec3's
    *
    * @param {Vec3} a the first operand
    * @param {Vec3} b the second operand
    * @returns {Float} distance between a and b
    */
    public static function distance( a : Vec3, b : Vec3 ) : Float {
    var x = b[0] - a[0];
    var y = b[1] - a[1];
    var z = b[2] - a[2];
    return Math.sqrt(x*x + y*y + z*z);
    }

    /**
    * Calculates the squared euclidian distance between two Vec3's
    *
    * @param {Vec3} a the first operand
    * @param {Vec3} b the second operand
    * @returns {Float} squared distance between a and b
    */
    public static function squaredDistance( a : Vec3, b : Vec3 ) : Float {
    var x = b[0] - a[0];
    var y = b[1] - a[1];
    var z = b[2] - a[2];
    return x*x + y*y + z*z;
    }

    /**
    * Calculates the squared length of a Vec3
    *
    * @param {Vec3} a vector to calculate squared length of
    * @returns {Float} squared length of a
    */
    public static function squaredLength( a : Vec3 ) : Float {
    var x = a[0];
    var y = a[1];
    var z = a[2];
    return x*x + y*y + z*z;
    }

    /**
    * Negates the components of a Vec3
    *
    * @param {Vec3} out the receiving vector
    * @param {Vec3} a vector to negate
    * @returns {Vec3} out
    */
    public static function negate( a : Vec3, ?out : Vec3 ) : Vec3 {
    if( out == null ) out = create();
    out[0] = -a[0];
    out[1] = -a[1];
    out[2] = -a[2];
    return out;
    }

    /**
    * Returns the inverse of the components of a Vec3
    *
    * @param {Vec3} out the receiving vector
    * @param {Vec3} a vector to invert
    * @returns {Vec3} out
    */
    public static function inverse( a : Vec3, ?out : Vec3 ) : Vec3 {
    if( out == null ) out = create();
    out[0] = 1.0 / a[0];
    out[1] = 1.0 / a[1];
    out[2] = 1.0 / a[2];
    return out;
    }

    /**
    * Normalize a Vec3
    *
    * @param {Vec3} out the receiving vector
    * @param {Vec3} a vector to normalize
    * @returns {Vec3} out
    */
    public static function normalize( a : Vec3, ?out : Vec3 ) : Vec3 {
    if( out == null ) out = create();
    var x = a[0];
    var y = a[1];
    var z = a[2];
    var len = x*x + y*y + z*z;
    if (len > 0) {
        //TODO: evaluate use of glm_invsqrt here?
        len = 1 / Math.sqrt(len);
    }
    out[0] = a[0] * len;
    out[1] = a[1] * len;
    out[2] = a[2] * len;
    return out;
    }

    /**
    * Calculates the dot product of two Vec3's
    *
    * @param {Vec3} a the first operand
    * @param {Vec3} b the second operand
    * @returns {Float} dot product of a and b
    */
    public static function dot( a : Vec3, b : Vec3 ) : Float {
    return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
    }

    /**
    * Computes the cross product of two Vec3's
    *
    * @param {Vec3} out the receiving vector
    * @param {Vec3} a the first operand
    * @param {Vec3} b the second operand
    * @returns {Vec3} out
    */
    public static function cross( a : Vec3, b : Vec3, ?out : Vec3 ) : Vec3 {
    if( out == null ) out = create();
    var ax = a[0], ay = a[1], az = a[2];
    var bx = b[0], by = b[1], bz = b[2];

    out[0] = ay * bz - az * by;
    out[1] = az * bx - ax * bz;
    out[2] = ax * by - ay * bx;
    return out;
    }

    /**
    * Performs a linear interpolation between two Vec3's
    *
    * @param {Vec3} out the receiving vector
    * @param {Vec3} a the first operand
    * @param {Vec3} b the second operand
    * @param {Float} t interpolation amount, in the range [0-1], between the two inputs
    * @returns {Vec3} out
    */
    public static function lerp( t : Float, a : Vec3, b : Vec3, ?out : Vec3 ) : Vec3 {
    if( out == null ) out = create();
    var ax = a[0];
    var ay = a[1];
    var az = a[2];
    out[0] = ax + t * (b[0] - ax);
    out[1] = ay + t * (b[1] - ay);
    out[2] = az + t * (b[2] - az);
    return out;
    }

    /**
    * Performs a hermite interpolation with two control points
    *
    * @param {Vec3} out the receiving vector
    * @param {Vec3} a the first operand
    * @param {Vec3} b the second operand
    * @param {Vec3} c the third operand
    * @param {Vec3} d the fourth operand
    * @param {Float} t interpolation amount, in the range [0-1], between the two inputs
    * @returns {Vec3} out
    */
    public static function hermite( t : Float, a : Vec3, b : Vec3, c : Vec3, d : Vec3, ?out : Vec3 ) : Vec3 {
    if( out == null ) out = create();
    var factorTimes2 = t * t;
    var factor1 = factorTimes2 * (2 * t - 3) + 1;
    var factor2 = factorTimes2 * (t - 2) + t;
    var factor3 = factorTimes2 * (t - 1);
    var factor4 = factorTimes2 * (3 - 2 * t);

    out[0] = a[0] * factor1 + b[0] * factor2 + c[0] * factor3 + d[0] * factor4;
    out[1] = a[1] * factor1 + b[1] * factor2 + c[1] * factor3 + d[1] * factor4;
    out[2] = a[2] * factor1 + b[2] * factor2 + c[2] * factor3 + d[2] * factor4;

    return out;
    }

    /**
    * Performs a bezier interpolation with two control points
    *
    * @param {Vec3} out the receiving vector
    * @param {Vec3} a the first operand
    * @param {Vec3} b the second operand
    * @param {Vec3} c the third operand
    * @param {Vec3} d the fourth operand
    * @param {Float} t interpolation amount, in the range [0-1], between the two inputs
    * @returns {Vec3} out
    */
    public static function bezier( t : Float, a : Vec3, b : Vec3, c : Vec3, d : Vec3, ?out : Vec3 ) : Vec3 {
    if( out == null ) out = create();
    var inverseFactor = 1 - t;
    var inverseFactorTimesTwo = inverseFactor * inverseFactor;
    var factorTimes2 = t * t;
    var factor1 = inverseFactorTimesTwo * inverseFactor;
    var factor2 = 3 * t * inverseFactorTimesTwo;
    var factor3 = 3 * factorTimes2 * inverseFactor;
    var factor4 = factorTimes2 * t;

    out[0] = a[0] * factor1 + b[0] * factor2 + c[0] * factor3 + d[0] * factor4;
    out[1] = a[1] * factor1 + b[1] * factor2 + c[1] * factor3 + d[1] * factor4;
    out[2] = a[2] * factor1 + b[2] * factor2 + c[2] * factor3 + d[2] * factor4;

    return out;
    }

    /**
    * Generates a random vector with the given scale
    *
    * @param {Vec3} out the receiving vector
    * @param {Float} [scale] Length of the resulting vector. If ommitted, a unit vector will be returned
    * @returns {Vec3} out
    */
    public static function random( ?out : Vec3, scale : Float = 1.0 ) : Vec3 {
    if( out == null ) out = create();
    var r = GLMatrix.RANDOM() * 2.0 * Math.PI;
    var z = (GLMatrix.RANDOM() * 2.0) - 1.0;
    var zScale = Math.sqrt(1.0-z*z) * scale;

    out[0] = Math.cos(r) * zScale;
    out[1] = Math.sin(r) * zScale;
    out[2] = z * scale;
    return out;
    }

    /**
    * Transforms the Vec3 with a Mat4.
    * 4th vector component is implicitly '1'
    *
    * @param {Vec3} out the receiving vector
    * @param {Vec3} a the vector to transform
    * @param {Mat4} m matrix to transform with
    * @returns {Vec3} out
    */
    public static function postmultiplyMat4( a : Vec3, m : Mat4, ?out : Vec3 ) : Vec3 {
    if( out == null ) out = create();
    var x = a[0], y = a[1], z = a[2];
    var w = m[3] * x + m[7] * y + m[11] * z + m[15];
    // w = w || 1.0;
    w = w == 0.0 ? 1.0 : w;
    out[0] = (m[0] * x + m[4] * y + m[8] * z + m[12]) / w;
    out[1] = (m[1] * x + m[5] * y + m[9] * z + m[13]) / w;
    out[2] = (m[2] * x + m[6] * y + m[10] * z + m[14]) / w;
    return out;
    }

    /**
    * Transforms the Vec3 with a Mat3.
    *
    * @param {Vec3} out the receiving vector
    * @param {Vec3} a the vector to transform
    * @param {Mat3} m the 3x3 matrix to transform with
    * @returns {Vec3} out
    */
    public static function postmultiplyMat3( a : Vec3, m : Mat3, ?out : Vec3 ) : Vec3 {
    if( out == null ) out = create();
    var x = a[0], y = a[1], z = a[2];
    out[0] = x * m[0] + y * m[3] + z * m[6];
    out[1] = x * m[1] + y * m[4] + z * m[7];
    out[2] = x * m[2] + y * m[5] + z * m[8];
    return out;
    }

    /**
    * Transforms the Vec3 with a Quat
    * Can also be used for dual quaternions. (Multiply it with the real part)
    *
    * @param {Vec3} out the receiving vector
    * @param {Vec3} a the vector to transform
    * @param {Quat} q quaternion to transform with
    * @returns {Vec3} out
    */
    public static function postmultiplyQuat( a : Vec3, q : Quat, ?out : Vec3 ) : Vec3 {
    if( out == null ) out = create();
        // benchmarks: https://jsperf.com/quaternion-transform-vec4-implementations-fixed
        var qx = q[0], qy = q[1], qz = q[2], qw = q[3];
        var x = a[0], y = a[1], z = a[2];
        // var qvec = [qx, qy, qz];
        // var uv = Vec3.cross([], qvec, a);
        var uvx = qy * z - qz * y,
            uvy = qz * x - qx * z,
            uvz = qx * y - qy * x;
        // var uuv = Vec3.cross([], qvec, uv);
        var uuvx = qy * uvz - qz * uvy,
            uuvy = qz * uvx - qx * uvz,
            uuvz = qx * uvy - qy * uvx;
        // Vec3.scale(uv, uv, 2 * w);
        var w2 = qw * 2;
        uvx *= w2;
        uvy *= w2;
        uvz *= w2;
        // Vec3.scale(uuv, uuv, 2);
        uuvx *= 2;
        uuvy *= 2;
        uuvz *= 2;
        // return Vec3.add(out, a, Vec3.add(out, uv, uuv));
        out[0] = x + uvx + uuvx;
        out[1] = y + uvy + uuvy;
        out[2] = z + uvz + uuvz;
        return out;
    }

    /**
    * Rotate a 3D vector around the x-axis
    * @param {Vec3} out The receiving Vec3
    * @param {Vec3} a The Vec3 point to rotate
    * @param {Vec3} b The origin of the rotation
    * @param {Float} c The angle of rotation
    * @returns {Vec3} out
    */
    public static function rotateX( a : Vec3, b : Vec3, c : Float, ?out : Vec3 ) : Vec3 {
    if( out == null ) out = create();
    var p = [], r=[];
    //Translate point to the origin
    p[0] = a[0] - b[0];
    p[1] = a[1] - b[1];
    p[2] = a[2] - b[2];

    //perform rotation
    r[0] = p[0];
    r[1] = p[1]*Math.cos(c) - p[2]*Math.sin(c);
    r[2] = p[1]*Math.sin(c) + p[2]*Math.cos(c);

    //translate to correct position
    out[0] = r[0] + b[0];
    out[1] = r[1] + b[1];
    out[2] = r[2] + b[2];

    return out;
    }

    /**
    * Rotate a 3D vector around the y-axis
    * @param {Vec3} out The receiving Vec3
    * @param {Vec3} a The Vec3 point to rotate
    * @param {Vec3} b The origin of the rotation
    * @param {Float} c The angle of rotation
    * @returns {Vec3} out
    */
    public static function rotateY( a : Vec3, b : Vec3, c : Float, ?out : Vec3 ) : Vec3 {
    if( out == null ) out = create();
    var p = [], r=[];
    //Translate point to the origin
    p[0] = a[0] - b[0];
    p[1] = a[1] - b[1];
    p[2] = a[2] - b[2];

    //perform rotation
    r[0] = p[2]*Math.sin(c) + p[0]*Math.cos(c);
    r[1] = p[1];
    r[2] = p[2]*Math.cos(c) - p[0]*Math.sin(c);

    //translate to correct position
    out[0] = r[0] + b[0];
    out[1] = r[1] + b[1];
    out[2] = r[2] + b[2];

    return out;
    }

    /**
    * Rotate a 3D vector around the z-axis
    * @param {Vec3} out The receiving Vec3
    * @param {Vec3} a The Vec3 point to rotate
    * @param {Vec3} b The origin of the rotation
    * @param {Float} c The angle of rotation
    * @returns {Vec3} out
    */
    public static function rotateZ( a : Vec3, b : Vec3, c : Float, ?out : Vec3 ) : Vec3 {
    if( out == null ) out = create();
    var p = [], r=[];
    //Translate point to the origin
    p[0] = a[0] - b[0];
    p[1] = a[1] - b[1];
    p[2] = a[2] - b[2];

    //perform rotation
    r[0] = p[0]*Math.cos(c) - p[1]*Math.sin(c);
    r[1] = p[0]*Math.sin(c) + p[1]*Math.cos(c);
    r[2] = p[2];

    //translate to correct position
    out[0] = r[0] + b[0];
    out[1] = r[1] + b[1];
    out[2] = r[2] + b[2];

    return out;
    }

    /**
    * Get the angle between two 3D vectors
    * @param {Vec3} a The first operand
    * @param {Vec3} b The second operand
    * @returns {Float} The angle in radians
    */
    public static function angle( a : Vec3, b : Vec3 ) : Float {
    var tempA = Vec3.fromValues(a[0], a[1], a[2]);
    var tempB = Vec3.fromValues(b[0], b[1], b[2]);

    normalize(tempA, tempA);
    normalize(tempB, tempB);

    var cosine = dot(tempA, tempB);

    if(cosine > 1.0) {
        return 0;
    }
    else if(cosine < -1.0) {
        return Math.PI;
    } else {
        return Math.acos(cosine);
    }
    }

    /**
    * Set the components of a Vec3 to zero
    *
    * @param {Vec3} out the receiving vector
    * @returns {Vec3} out
    */
    public static function zero( out : Vec3 ) : Vec3 {
    out[0] = 0.0;
    out[1] = 0.0;
    out[2] = 0.0;
    return out;
    }

    /**
    * Returns a string representation of a vector
    *
    * @param {Vec3} a vector to represent as a string
    * @returns {String} string representation of the vector
    */
    public static function str( a : Vec3 ) : String {
    return 'Vec3(' + a[0] + ', ' + a[1] + ', ' + a[2] + ')';
    }

    /**
    * Returns whether or not the vectors have exactly the same elements in the same position (when compared with ==)
    *
    * @param {Vec3} a The first vector.
    * @param {Vec3} b The second vector.
    * @returns {Boolean} True if the vectors are equal, false otherwise.
    */
    public static function exactEquals( a : Vec3, b : Vec3 ) : Bool {
    return a[0] == b[0] && a[1] == b[1] && a[2] == b[2];
    }

    /**
    * Returns whether or not the vectors have approximately the same elements in the same position.
    *
    * @param {Vec3} a The first vector.
    * @param {Vec3} b The second vector.
    * @returns {Boolean} True if the vectors are equal, false otherwise.
    */
    public static function equals( a : Vec3, b : Vec3 ) : Bool {
    var a0 = a[0], a1 = a[1], a2 = a[2];
    var b0 = b[0], b1 = b[1], b2 = b[2];
    return (Math.abs(a0 - b0) <= GLMatrix.EPSILON*Math.max(1.0, Math.max(Math.abs(a0), Math.abs(b0))) &&
            Math.abs(a1 - b1) <= GLMatrix.EPSILON*Math.max(1.0, Math.max(Math.abs(a1), Math.abs(b1))) &&
            Math.abs(a2 - b2) <= GLMatrix.EPSILON*Math.max(1.0, Math.max(Math.abs(a2), Math.abs(b2))));
    }

    /**
    * Alias for {@link Vec3.subtract}
    * @function
    */
    public static var sub = subtract;

    /**
    * Alias for {@link Vec3.multiply}
    * @function
    */
    public static var mul = multiply;

    /**
    * Alias for {@link Vec3.divide}
    * @function
    */
    public static var div = divide;

    /**
    * Alias for {@link Vec3.distance}
    * @function
    */
    public static var dist = distance;

    /**
    * Alias for {@link Vec3.squaredDistance}
    * @function
    */
    public static var sqrDist = squaredDistance;

    /**
    * Alias for {@link Vec3.length}
    * @function
    */
    public static var len = length;

    /**
    * Alias for {@link Vec3.squaredLength}
    * @function
    */
    public static var sqrLen = squaredLength;
}
