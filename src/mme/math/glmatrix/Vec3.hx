package mme.math.glmatrix;

import mme.math.glmatrix.Vec3Tools;

#if lime
import lime.utils.Float32Array;
abstract Vec3(Float32Array) from Float32Array to Float32Array {
#else
import haxe.ds.Vector;
abstract Vec3(Vector<Float>) from Vector<Float> to Vector<Float> {
#end

	public static var X_AXIS( get, never ) : Vec3;
	public static var Y_AXIS( get, never ) : Vec3;
	public static var Z_AXIS( get, never ) : Vec3;

    public inline function new() {
        #if lime
        this = new Float32Array(3);
        #else
        this = new Vector<Float>(3);
        this[0] = 0.0;
        this[1] = 0.0;
        this[2] = 0.0;
        #end
    }
    @:arrayAccess
    public inline function getk(key:Int) {
    return this[key];
    }
    @:arrayAccess
    public inline function setk(k:Int, v:Float):Float {
        this[k] = v;
        return v;
    }

    public var x ( get, set ) : Float;
    public var y ( get, set ) : Float;
    public var z ( get, set ) : Float;
    private inline function get_x() : Float {
        return this[0];
    }
    private inline function set_x( x : Float) : Float {
        return this[0] = x;
    }
    private inline function get_y() : Float {
        return this[1];
    }
    private inline function set_y( y : Float) : Float {
        return this[1] = y;
    }
    private inline function get_z() : Float {
        return this[2];
    }
    private inline function set_z( z : Float) : Float {
        return this[2] = z;
    }

    @:from
    static public function fromArray(a:Array<Float>) {
        return fromValues( a[0], a[1], a[2] );
    }
    @:to
    public function toArray() {
        return [this[0], this[1], this[2]];
    }
    public function toString() : String {
        return Vec3Tools.str(this);
    }

	private inline static function get_X_AXIS() : Vec3 {
		return [1, 0, 0];
	}
	private inline static function get_Y_AXIS() : Vec3 {
		return [0, 1, 0];
	}
	private inline static function get_Z_AXIS() : Vec3 {
		return [0, 0, 1];
	}

    /**
    * Creates a new Vec3 initialized with the given values
    *
    * @param x X component
    * @param y Y component
    * @param z Z component
    * @returns a new 3D vector
    */
    public static function fromValues( ?out : Vec3, x : Float, y : Float, z : Float ) : Vec3 {
        if( out == null ) out = Vec3Tools.create();
        out[0] = x;
        out[1] = y;
        out[2] = z;
        return out;
    }
}
