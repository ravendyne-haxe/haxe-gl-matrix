package mme.math.glmatrix;

import mme.math.glmatrix.GLMatrix;

import mme.math.glmatrix.Mat3;
import mme.math.glmatrix.Vec2;
import mme.math.glmatrix.Mat4;


/**
 * 3x3 Matrix
 * @module Mat3
 */
class Mat3Tools {

/*

//
// GENERATORS in Mat3 class
//

Mat3.fromValues( ?out : Mat3, m00 : Float, m01 : Float, m02 : Float, m10 : Float, m11 : Float, m12 : Float, m20 : Float, m21 : Float, m22 : Float ) : Mat3;
Mat3.fromMat4( ?out : Mat3, a : Mat4 ) : Mat3;
Mat3.fromTranslation( ?out : Mat3, v : Vec2 ) : Mat3;
Mat3.fromRotation( ?out : Mat3, rad : Float ) : Mat3;
Mat3.fromScaling( ?out : Mat3, v : Vec2 ) : Mat3;
Mat3.fromMat2d( ?out : Mat3, a : Mat2d ) : Mat3;
Mat3.fromQuat( ?out : Mat3, q : Quat ) : Mat3;


//
// GENERATORS
//
create() : Mat3;
mat3.clone( a : Mat3 ) : Mat3;
?mat3.identity( ?out : Mat3 ) : Mat3;

?mat3.normalFromMat4( ?out : Mat3, a : Mat4 ) : Mat3;

projection( width : Float, height : Float, ?out : Mat3 ) : Mat3;


//
// EDIT
//
mat3.set( out : Mat3, m00 : Float, m01 : Float, m02 : Float, m10 : Float, m11 : Float, m12 : Float, m20 : Float, m21 : Float, m22 : Float ) : Mat3;


//
// OPERATORS
//

// unary
//
mat3.copy( a : Mat3, ?out : Mat3 ) : Mat3;
mat3.transpose( a : Mat3, ?out : Mat3 ) : Mat3;
mat3.invert( a : Mat3, ?out : Mat3 ) : Mat3;
mat3.adjoint( a : Mat3, ?out : Mat3 ) : Mat3;

// unary, non-Mat3 result
//
mat3.determinant( a : Mat3 ) : Float;
mat3.str( a : Mat3 ) : String;
mat3.frob( a : Mat3 ) : Float;


// binary
//
mat3.multiply( a : Mat3, b : Mat3, ?out : Mat3 ) : Mat3;
mat3.add( a : Mat3, b : Mat3, ?out : Mat3 ) : Mat3;
mat3.subtract( a : Mat3, b : Mat3, ?out : Mat3 ) : Mat3;


// binary, non-Mat3 arg
//
mat3.multiplyVec3( a : Mat3, v : Vec3, ?out : Vec3 ) : Vec3;
mat3.scale( a : Mat3, v : Vec2, ?out : Mat3 ) : Mat3;
mat3.translate( a : Mat3, v : Vec2, ?out : Mat3 ) : Mat3;
mat3.rotate( a : Mat3, rad : Float, ?out : Mat3 ) : Mat3;
mat3.multiplyScalar( a : Mat3, b : Float, ?out : Mat3 ) : Mat3;
mat3.multiplyVec2( a : Mat3, v : Vec2, ?out : Vec2 ) : Vec2;


// binary, non-Mat3 result
//
mat3.exactEquals( a : Mat3, b : Mat3 ) : Bool;
mat3.equals( a : Mat3, b : Mat3 ) : Bool;


// ternary
//
mat3.multiplyScalarAndAdd( a : Mat3, b : Mat3, scale : Float, ?out : Mat3 ) : Mat3;

*/

    /**
    * Creates a new identity Mat3
    *
    * @returns {Mat3} a new 3x3 matrix
    */
    public static inline function create() : Mat3 {
    var out = new Mat3();
    return out;
    }

    /**
    * Creates a new Mat3 initialized with values from an existing matrix
    *
    * @param {Mat3} a matrix to clone
    * @returns {Mat3} a new 3x3 matrix
    */
    public static function clone( a : Mat3 ) : Mat3 {
    var out = new Mat3();
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
    out[4] = a[4];
    out[5] = a[5];
    out[6] = a[6];
    out[7] = a[7];
    out[8] = a[8];
    return out;
    }

    /**
    * Copy the values from one Mat3 to another
    *
    * @param {Mat3} out the receiving matrix
    * @param {Mat3} a the source matrix
    * @returns {Mat3} out
    */
    public static function copy( a : Mat3, ?out : Mat3 ) : Mat3 {
    if( out == null ) out = create();
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
    out[4] = a[4];
    out[5] = a[5];
    out[6] = a[6];
    out[7] = a[7];
    out[8] = a[8];
    return out;
    }

    /**
    * Set the components of a Mat3 to the given values
    *
    * @param {Mat3} out the receiving matrix
    * @param {Float} m00 Component in column 0, row 0 position (index 0)
    * @param {Float} m01 Component in column 0, row 1 position (index 1)
    * @param {Float} m02 Component in column 0, row 2 position (index 2)
    * @param {Float} m10 Component in column 1, row 0 position (index 3)
    * @param {Float} m11 Component in column 1, row 1 position (index 4)
    * @param {Float} m12 Component in column 1, row 2 position (index 5)
    * @param {Float} m20 Component in column 2, row 0 position (index 6)
    * @param {Float} m21 Component in column 2, row 1 position (index 7)
    * @param {Float} m22 Component in column 2, row 2 position (index 8)
    * @returns {Mat3} out
    */
    public static function set( out : Mat3, m00 : Float, m01 : Float, m02 : Float, m10 : Float, m11 : Float, m12 : Float, m20 : Float, m21 : Float, m22 : Float ) : Mat3 {
    out[0] = m00;
    out[1] = m01;
    out[2] = m02;
    out[3] = m10;
    out[4] = m11;
    out[5] = m12;
    out[6] = m20;
    out[7] = m21;
    out[8] = m22;
    return out;
    }

    /**
    * Set a Mat3 to the identity matrix
    *
    * @param {Mat3} out the receiving matrix
    * @returns {Mat3} out
    */
    public static function identity( ?out : Mat3 ) : Mat3 {
    if( out == null ) out = create();
    out[0] = 1;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 1;
    out[5] = 0;
    out[6] = 0;
    out[7] = 0;
    out[8] = 1;
    return out;
    }

    /**
    * Transpose the values of a Mat3
    *
    * @param {Mat3} out the receiving matrix
    * @param {Mat3} a the source matrix
    * @returns {Mat3} out
    */
    public static function transpose( a : Mat3, ?out : Mat3 ) : Mat3 {
    if( out == null ) out = create();
    // If we are transposing ourselves we can skip a few steps but have to cache some values
    if (out == a) {
        var a01 = a[1], a02 = a[2], a12 = a[5];
        out[1] = a[3];
        out[2] = a[6];
        out[3] = a01;
        out[5] = a[7];
        out[6] = a02;
        out[7] = a12;
    } else {
        out[0] = a[0];
        out[1] = a[3];
        out[2] = a[6];
        out[3] = a[1];
        out[4] = a[4];
        out[5] = a[7];
        out[6] = a[2];
        out[7] = a[5];
        out[8] = a[8];
    }

    return out;
    }

    /**
    * Inverts a Mat3
    *
    * @param {Mat3} out the receiving matrix
    * @param {Mat3} a the source matrix
    * @returns {Mat3} out
    */
    public static function invert( a : Mat3, ?out : Mat3 ) : Mat3 {
    if( out == null ) out = create();
    var a00 = a[0], a01 = a[1], a02 = a[2];
    var a10 = a[3], a11 = a[4], a12 = a[5];
    var a20 = a[6], a21 = a[7], a22 = a[8];

    var b01 = a22 * a11 - a12 * a21;
    var b11 = -a22 * a10 + a12 * a20;
    var b21 = a21 * a10 - a11 * a20;

    // Calculate the determinant
    var det = a00 * b01 + a01 * b11 + a02 * b21;

    if ( det == 0.0 ) {
        return null;
    }
    det = 1.0 / det;

    out[0] = b01 * det;
    out[1] = (-a22 * a01 + a02 * a21) * det;
    out[2] = (a12 * a01 - a02 * a11) * det;
    out[3] = b11 * det;
    out[4] = (a22 * a00 - a02 * a20) * det;
    out[5] = (-a12 * a00 + a02 * a10) * det;
    out[6] = b21 * det;
    out[7] = (-a21 * a00 + a01 * a20) * det;
    out[8] = (a11 * a00 - a01 * a10) * det;
    return out;
    }

    /**
    * Calculates the adjugate of a Mat3
    *
    * @param {Mat3} out the receiving matrix
    * @param {Mat3} a the source matrix
    * @returns {Mat3} out
    */
    public static function adjoint( a : Mat3, ?out : Mat3 ) : Mat3 {
    if( out == null ) out = create();
    var a00 = a[0], a01 = a[1], a02 = a[2];
    var a10 = a[3], a11 = a[4], a12 = a[5];
    var a20 = a[6], a21 = a[7], a22 = a[8];

    out[0] = (a11 * a22 - a12 * a21);
    out[1] = (a02 * a21 - a01 * a22);
    out[2] = (a01 * a12 - a02 * a11);
    out[3] = (a12 * a20 - a10 * a22);
    out[4] = (a00 * a22 - a02 * a20);
    out[5] = (a02 * a10 - a00 * a12);
    out[6] = (a10 * a21 - a11 * a20);
    out[7] = (a01 * a20 - a00 * a21);
    out[8] = (a00 * a11 - a01 * a10);
    return out;
    }

    /**
    * Calculates the determinant of a Mat3
    *
    * @param {Mat3} a the source matrix
    * @returns {Float} determinant of a
    */
    public static function determinant( a : Mat3 ) : Float {
    var a00 = a[0], a01 = a[1], a02 = a[2];
    var a10 = a[3], a11 = a[4], a12 = a[5];
    var a20 = a[6], a21 = a[7], a22 = a[8];

    return a00 * (a22 * a11 - a12 * a21) + a01 * (-a22 * a10 + a12 * a20) + a02 * (a21 * a10 - a11 * a20);
    }

    /**
    * Multiplies two Mat3's
    *
    * @param {Mat3} out the receiving matrix
    * @param {Mat3} a the first operand
    * @param {Mat3} b the second operand
    * @returns {Mat3} out
    */
    public static function multiply( a : Mat3, b : Mat3, ?out : Mat3 ) : Mat3 {
    if( out == null ) out = create();
    var a00 = a[0], a01 = a[1], a02 = a[2];
    var a10 = a[3], a11 = a[4], a12 = a[5];
    var a20 = a[6], a21 = a[7], a22 = a[8];

    var b00 = b[0], b01 = b[1], b02 = b[2];
    var b10 = b[3], b11 = b[4], b12 = b[5];
    var b20 = b[6], b21 = b[7], b22 = b[8];

    out[0] = b00 * a00 + b01 * a10 + b02 * a20;
    out[1] = b00 * a01 + b01 * a11 + b02 * a21;
    out[2] = b00 * a02 + b01 * a12 + b02 * a22;

    out[3] = b10 * a00 + b11 * a10 + b12 * a20;
    out[4] = b10 * a01 + b11 * a11 + b12 * a21;
    out[5] = b10 * a02 + b11 * a12 + b12 * a22;

    out[6] = b20 * a00 + b21 * a10 + b22 * a20;
    out[7] = b20 * a01 + b21 * a11 + b22 * a21;
    out[8] = b20 * a02 + b21 * a12 + b22 * a22;
    return out;
    }

    /**
    * Translate a Mat3 by the given vector
    *
    * @param {Mat3} out the receiving matrix
    * @param {Mat3} a the matrix to translate
    * @param {Vec2} v vector to translate by
    * @returns {Mat3} out
    */
    public static function translate( a : Mat3, v : Vec2, ?out : Mat3 ) : Mat3 {
    if( out == null ) out = create();
    var a00 = a[0], a01 = a[1], a02 = a[2],
        a10 = a[3], a11 = a[4], a12 = a[5],
        a20 = a[6], a21 = a[7], a22 = a[8],
        x = v[0], y = v[1];

    out[0] = a00;
    out[1] = a01;
    out[2] = a02;

    out[3] = a10;
    out[4] = a11;
    out[5] = a12;

    out[6] = x * a00 + y * a10 + a20;
    out[7] = x * a01 + y * a11 + a21;
    out[8] = x * a02 + y * a12 + a22;
    return out;
    }

    /**
    * Rotates a Mat3 by the given angle
    *
    * @param {Mat3} out the receiving matrix
    * @param {Mat3} a the matrix to rotate
    * @param {Float} rad the angle to rotate the matrix by
    * @returns {Mat3} out
    */
    public static function rotate( a : Mat3, rad : Float, ?out : Mat3 ) : Mat3 {
    if( out == null ) out = create();
    var a00 = a[0], a01 = a[1], a02 = a[2],
        a10 = a[3], a11 = a[4], a12 = a[5],
        a20 = a[6], a21 = a[7], a22 = a[8],

        s = Math.sin(rad),
        c = Math.cos(rad);

    out[0] = c * a00 + s * a10;
    out[1] = c * a01 + s * a11;
    out[2] = c * a02 + s * a12;

    out[3] = c * a10 - s * a00;
    out[4] = c * a11 - s * a01;
    out[5] = c * a12 - s * a02;

    out[6] = a20;
    out[7] = a21;
    out[8] = a22;
    return out;
    };

    /**
    * Scales the Mat3 by the dimensions in the given Vec2
    *
    * @param {Mat3} out the receiving matrix
    * @param {Mat3} a the matrix to rotate
    * @param {Vec2} v the Vec2 to scale the matrix by
    * @returns {Mat3} out
    **/
    public static function scale( a : Mat3, v : Vec2, ?out : Mat3 ) : Mat3 {
    if( out == null ) out = create();
    var x = v[0], y = v[1];

    out[0] = x * a[0];
    out[1] = x * a[1];
    out[2] = x * a[2];

    out[3] = y * a[3];
    out[4] = y * a[4];
    out[5] = y * a[5];

    out[6] = a[6];
    out[7] = a[7];
    out[8] = a[8];
    return out;
    }

    /**
    * Calculates a 3x3 normal matrix (transpose inverse) from the 4x4 matrix
    *
    * @param {Mat3} out Mat3 receiving operation result
    * @param {Mat4} a Mat4 to derive the normal matrix from
    *
    * @returns {Mat3} out
    */
    public static function normalFromMat4( ?out : Mat3, a : Mat4 ) : Mat3 {
    if( out == null ) out = create();
    var a00 = a[0], a01 = a[1], a02 = a[2], a03 = a[3];
    var a10 = a[4], a11 = a[5], a12 = a[6], a13 = a[7];
    var a20 = a[8], a21 = a[9], a22 = a[10], a23 = a[11];
    var a30 = a[12], a31 = a[13], a32 = a[14], a33 = a[15];

    var b00 = a00 * a11 - a01 * a10;
    var b01 = a00 * a12 - a02 * a10;
    var b02 = a00 * a13 - a03 * a10;
    var b03 = a01 * a12 - a02 * a11;
    var b04 = a01 * a13 - a03 * a11;
    var b05 = a02 * a13 - a03 * a12;
    var b06 = a20 * a31 - a21 * a30;
    var b07 = a20 * a32 - a22 * a30;
    var b08 = a20 * a33 - a23 * a30;
    var b09 = a21 * a32 - a22 * a31;
    var b10 = a21 * a33 - a23 * a31;
    var b11 = a22 * a33 - a23 * a32;

    // Calculate the determinant
    var det = b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06;

    if ( det == 0.0 ) {
        return null;
    }
    det = 1.0 / det;

    out[0] = (a11 * b11 - a12 * b10 + a13 * b09) * det;
    out[1] = (a12 * b08 - a10 * b11 - a13 * b07) * det;
    out[2] = (a10 * b10 - a11 * b08 + a13 * b06) * det;

    out[3] = (a02 * b10 - a01 * b11 - a03 * b09) * det;
    out[4] = (a00 * b11 - a02 * b08 + a03 * b07) * det;
    out[5] = (a01 * b08 - a00 * b10 - a03 * b06) * det;

    out[6] = (a31 * b05 - a32 * b04 + a33 * b03) * det;
    out[7] = (a32 * b02 - a30 * b05 - a33 * b01) * det;
    out[8] = (a30 * b04 - a31 * b02 + a33 * b00) * det;

    return out;
    }

    /**
    * Generates a 2D projection matrix with the given bounds
    *
    * @param {Mat3} out Mat3 frustum matrix will be written into
    * @param {Float} width Width of your gl context
    * @param {Float} height Height of gl context
    * @returns {Mat3} out
    */
    public static function projection( width : Float, height : Float, ?out : Mat3 ) : Mat3 {
    if( out == null ) out = create();
        out[0] = 2 / width;
        out[1] = 0;
        out[2] = 0;
        out[3] = 0;
        out[4] = -2 / height;
        out[5] = 0;
        out[6] = -1;
        out[7] = 1;
        out[8] = 1;
        return out;
    }

    /**
    * Returns a string representation of a Mat3
    *
    * @param {Mat3} a matrix to represent as a string
    * @returns {String} string representation of the matrix
    */
    public static function str( a : Mat3 ) : String {
    return 'Mat3(' + a[0] + ', ' + a[1] + ', ' + a[2] + ', ' +
            a[3] + ', ' + a[4] + ', ' + a[5] + ', ' +
            a[6] + ', ' + a[7] + ', ' + a[8] + ')';
    }

    /**
    * Returns Frobenius norm of a Mat3
    *
    * @param {Mat3} a the matrix to calculate Frobenius norm of
    * @returns {Float} Frobenius norm
    */
    public static function frob( a : Mat3 ) : Float {
    return(Math.sqrt(Math.pow(a[0], 2) + Math.pow(a[1], 2) + Math.pow(a[2], 2) + Math.pow(a[3], 2) + Math.pow(a[4], 2) + Math.pow(a[5], 2) + Math.pow(a[6], 2) + Math.pow(a[7], 2) + Math.pow(a[8], 2)));
    }

    /**
    * Adds two Mat3's
    *
    * @param {Mat3} out the receiving matrix
    * @param {Mat3} a the first operand
    * @param {Mat3} b the second operand
    * @returns {Mat3} out
    */
    public static function add( a : Mat3, b : Mat3, ?out : Mat3 ) : Mat3 {
    if( out == null ) out = create();
    out[0] = a[0] + b[0];
    out[1] = a[1] + b[1];
    out[2] = a[2] + b[2];
    out[3] = a[3] + b[3];
    out[4] = a[4] + b[4];
    out[5] = a[5] + b[5];
    out[6] = a[6] + b[6];
    out[7] = a[7] + b[7];
    out[8] = a[8] + b[8];
    return out;
    }

    /**
    * Subtracts matrix b from matrix a
    *
    * @param {Mat3} out the receiving matrix
    * @param {Mat3} a the first operand
    * @param {Mat3} b the second operand
    * @returns {Mat3} out
    */
    public static function subtract( a : Mat3, b : Mat3, ?out : Mat3 ) : Mat3 {
    if( out == null ) out = create();
    out[0] = a[0] - b[0];
    out[1] = a[1] - b[1];
    out[2] = a[2] - b[2];
    out[3] = a[3] - b[3];
    out[4] = a[4] - b[4];
    out[5] = a[5] - b[5];
    out[6] = a[6] - b[6];
    out[7] = a[7] - b[7];
    out[8] = a[8] - b[8];
    return out;
    }



    /**
    * Multiply Mat3 with Vec3.
    *
    * @param {Vec3} out the receiving vector
    * @param {Mat3} m the 3x3 matrix to be multiplied by vector
    * @param {Vec3} a the vector to multiply matrix with
    * @returns {Vec3} out
    */
    public static function multiplyVec3( a : Mat3, v : Vec3, ?out : Vec3 ) : Vec3 {
    if( out == null ) out = Vec3Tools.create();
    var x = v[0], y = v[1], z = v[2];
    out[0] = x * a[0] + y * a[3] + z * a[6];
    out[1] = x * a[1] + y * a[4] + z * a[7];
    out[2] = x * a[2] + y * a[5] + z * a[8];
    return out;
    }

    /**
    * Multiply Mat3 with Vec2
    * 3rd vector component is implicitly '1'
    * out = m * a
    *
    * @param {Vec2} out the receiving vector
    * @param {Mat3} m matrix to be multiplied by vector
    * @param {Vec2} a the vector to multiply matrix with
    * @returns {Vec2} out
    */
    public static function multiplyVec2( a : Mat3, v : Vec2, ?out : Vec2 ) : Vec2 {
    if( out == null ) out = Vec2Tools.create();
    var x = v[0],
        y = v[1];
    out[0] = a[0] * x + a[3] * y + a[6];
    out[1] = a[1] * x + a[4] * y + a[7];
    return out;
    }

    /**
    * Multiply each element of the matrix by a scalar.
    *
    * @param {Mat3} out the receiving matrix
    * @param {Mat3} a the matrix to scale
    * @param {Float} b amount to scale the matrix's elements by
    * @returns {Mat3} out
    */
    public static function multiplyScalar( a : Mat3, b : Float, ?out : Mat3 ) : Mat3 {
    if( out == null ) out = create();
    out[0] = a[0] * b;
    out[1] = a[1] * b;
    out[2] = a[2] * b;
    out[3] = a[3] * b;
    out[4] = a[4] * b;
    out[5] = a[5] * b;
    out[6] = a[6] * b;
    out[7] = a[7] * b;
    out[8] = a[8] * b;
    return out;
    }

    /**
    * Adds two Mat3's after multiplying each element of the second operand by a scalar value.
    *
    * @param {Mat3} out the receiving vector
    * @param {Mat3} a the first operand
    * @param {Mat3} b the second operand
    * @param {Float} scale the amount to scale b's elements by before adding
    * @returns {Mat3} out
    */
    public static function multiplyScalarAndAdd( a : Mat3, b : Mat3, scale : Float, ?out : Mat3 ) : Mat3 {
    if( out == null ) out = create();
    out[0] = a[0] + (b[0] * scale);
    out[1] = a[1] + (b[1] * scale);
    out[2] = a[2] + (b[2] * scale);
    out[3] = a[3] + (b[3] * scale);
    out[4] = a[4] + (b[4] * scale);
    out[5] = a[5] + (b[5] * scale);
    out[6] = a[6] + (b[6] * scale);
    out[7] = a[7] + (b[7] * scale);
    out[8] = a[8] + (b[8] * scale);
    return out;
    }

    /**
    * Returns whether or not the matrices have exactly the same elements in the same position (when compared with ==)
    *
    * @param {Mat3} a The first matrix.
    * @param {Mat3} b The second matrix.
    * @returns {Boolean} True if the matrices are equal, false otherwise.
    */
    public static function exactEquals( a : Mat3, b : Mat3 ) : Bool {
    return a[0] == b[0] && a[1] == b[1] && a[2] == b[2] &&
            a[3] == b[3] && a[4] == b[4] && a[5] == b[5] &&
            a[6] == b[6] && a[7] == b[7] && a[8] == b[8];
    }

    /**
    * Returns whether or not the matrices have approximately the same elements in the same position.
    *
    * @param {Mat3} a The first matrix.
    * @param {Mat3} b The second matrix.
    * @returns {Boolean} True if the matrices are equal, false otherwise.
    */
    public static function equals( a : Mat3, b : Mat3 ) : Bool {
    var a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3], a4 = a[4], a5 = a[5], a6 = a[6], a7 = a[7], a8 = a[8];
    var b0 = b[0], b1 = b[1], b2 = b[2], b3 = b[3], b4 = b[4], b5 = b[5], b6 = b[6], b7 = b[7], b8 = b[8];
    return (Math.abs(a0 - b0) <= GLMatrix.EPSILON*Math.max(1.0, Math.max(Math.abs(a0), Math.abs(b0))) &&
            Math.abs(a1 - b1) <= GLMatrix.EPSILON*Math.max(1.0, Math.max(Math.abs(a1), Math.abs(b1))) &&
            Math.abs(a2 - b2) <= GLMatrix.EPSILON*Math.max(1.0, Math.max(Math.abs(a2), Math.abs(b2))) &&
            Math.abs(a3 - b3) <= GLMatrix.EPSILON*Math.max(1.0, Math.max(Math.abs(a3), Math.abs(b3))) &&
            Math.abs(a4 - b4) <= GLMatrix.EPSILON*Math.max(1.0, Math.max(Math.abs(a4), Math.abs(b4))) &&
            Math.abs(a5 - b5) <= GLMatrix.EPSILON*Math.max(1.0, Math.max(Math.abs(a5), Math.abs(b5))) &&
            Math.abs(a6 - b6) <= GLMatrix.EPSILON*Math.max(1.0, Math.max(Math.abs(a6), Math.abs(b6))) &&
            Math.abs(a7 - b7) <= GLMatrix.EPSILON*Math.max(1.0, Math.max(Math.abs(a7), Math.abs(b7))) &&
            Math.abs(a8 - b8) <= GLMatrix.EPSILON*Math.max(1.0, Math.max(Math.abs(a8), Math.abs(b8))));
    }

    /**
    * Alias for {@link Mat3.multiply}
    * @function
    */
    public static var mul = multiply;

    /**
    * Alias for {@link Mat3.subtract}
    * @function
    */
    public static var sub = subtract;
}
