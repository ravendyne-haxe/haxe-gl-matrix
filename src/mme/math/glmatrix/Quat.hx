package mme.math.glmatrix;

import mme.math.glmatrix.QuatTools;

#if lime
import lime.utils.Float32Array;
abstract Quat(Float32Array) from Float32Array to Float32Array {
#else
import haxe.ds.Vector;
abstract Quat(Vector<Float>) from Vector<Float> to Vector<Float> {
#end
    public inline function new() {
        #if lime
        this = new Float32Array(9);
        #else
        this = new Vector<Float>(9);
        this[0] = 0.0;
        this[1] = 0.0;
        this[2] = 0.0;
        #end
        this[3] = 1.0;
    }
    @:arrayAccess
    public inline function getk(key:Int) {
    return this[key];
    }
    @:arrayAccess
    public inline function setk(k:Int, v:Float):Float {
        this[k] = v;
        return v;
    }

    public var x ( get, set ) : Float;
    public var y ( get, set ) : Float;
    public var z ( get, set ) : Float;
    public var w ( get, set ) : Float;
    private inline function get_x() : Float {
        return this[0];
    }
    private inline function set_x( x : Float) : Float {
        return this[0] = x;
    }
    private inline function get_y() : Float {
        return this[1];
    }
    private inline function set_y( y : Float) : Float {
        return this[1] = y;
    }
    private inline function get_z() : Float {
        return this[2];
    }
    private inline function set_z( z : Float) : Float {
        return this[2] = z;
    }
    private inline function get_w() : Float {
        return this[3];
    }
    private inline function set_w( w : Float) : Float {
        return this[3] = w;
    }

    @:from
    static public function fromArray(a:Array<Float>) {
        return fromValues( a[0], a[1], a[2], a[3] );
    }
    @:to
    public function toArray() {
        return [this[0], this[1], this[2], this[3], this[4]];
    }
    public function toString() : String {
        return QuatTools.str(this);
    }

    /**
    * Creates a new Quat initialized with the given values
    *
    * @param x X component
    * @param y Y component
    * @param z Z component
    * @param w W component
    * @returns a new quaternion
    * @function
    */
    public static function fromValues( ?out : Quat, x : Float, y : Float, z : Float, w : Float ) : Quat {
        if( out == null ) out = QuatTools.create();
        out[0] = x;
        out[1] = y;
        out[2] = z;
        out[3] = w;
        return out;
    }

    /**
    * Sets a Quat from the given angle and rotation axis,
    * then returns it.
    *
    * @param out the receiving quaternion
    * @param axis the axis around which to rotate
    * @param rad the angle in radians
    * @returns out
    **/
    public static function fromAxisAngle( ?out : Quat, axis : Vec3, rad : Float ) : Quat {
        if( out == null ) out = QuatTools.create();
        rad = rad * 0.5;
        var s = Math.sin(rad);
        out[0] = s * axis[0];
        out[1] = s * axis[1];
        out[2] = s * axis[2];
        out[3] = Math.cos(rad);
        return out;
    }

    /**
    * Sets the specified quaternion with values corresponding to the given
    * axes. Each axis is a vec3 and is expected to be unit length and
    * perpendicular to all other specified axes.
    *
    * @param view  the vector representing the viewing direction
    * @param right the vector representing the local "right" direction
    * @param up    the vector representing the local "up" direction
    * @returns out
    */
    public static function fromAxes( ?out : Quat, view : Vec3, right : Vec3, up : Vec3 ) : Quat {
       var matr = Mat3Tools.create();

    // return function(out, view, right, up) {
        if( out == null ) out = QuatTools.create();
        matr[0] = right[0];
        matr[3] = right[1];
        matr[6] = right[2];

        matr[1] = up[0];
        matr[4] = up[1];
        matr[7] = up[2];

        matr[2] = -view[0];
        matr[5] = -view[1];
        matr[8] = -view[2];

        return QuatTools.normalize(out, fromMat3(out, matr));
    };
    // })();

    /**
    * Creates a quaternion from the given 3x3 rotation matrix.
    *
    * NOTE: The resultant quaternion is not normalized, so you should be sure
    * to renormalize the quaternion yourself where necessary.
    *
    * @param out the receiving quaternion
    * @param m rotation matrix
    * @returns out
    * @function
    */
    public static function fromMat3( ?out : Quat, m : Mat3 ) : Quat {
        if( out == null ) out = QuatTools.create();
        // Algorithm in Ken Shoemake's article in 1987 SIGGRAPH course notes
        // article "Quaternion Calculus and Fast Animation".
        var fTrace = m[0] + m[4] + m[8];
        var fRoot;

        if ( fTrace > 0.0 ) {
            // |w| > 1/2, may as well choose w > 1/2
            fRoot = Math.sqrt(fTrace + 1.0);  // 2w
            out[3] = 0.5 * fRoot;
            fRoot = 0.5/fRoot;  // 1/(4w)
            out[0] = (m[5]-m[7])*fRoot;
            out[1] = (m[6]-m[2])*fRoot;
            out[2] = (m[1]-m[3])*fRoot;
        } else {
            // |w| <= 1/2
            var i = 0;
            if ( m[4] > m[0] )
            i = 1;
            if ( m[8] > m[i*3+i] )
            i = 2;
            var j = (i+1)%3;
            var k = (i+2)%3;

            fRoot = Math.sqrt(m[i*3+i]-m[j*3+j]-m[k*3+k] + 1.0);
            out[i] = 0.5 * fRoot;
            fRoot = 0.5 / fRoot;
            out[3] = (m[j*3+k] - m[k*3+j]) * fRoot;
            out[j] = (m[j*3+i] + m[i*3+j]) * fRoot;
            out[k] = (m[k*3+i] + m[i*3+k]) * fRoot;
        }

        return out;
    }

    /**
    * Creates a quaternion from the given euler angle x, y, z.
    *
    * @param out the receiving quaternion
    * @param Angle to rotate around X axis in degrees.
    * @param Angle to rotate around Y axis in degrees.
    * @param Angle to rotate around Z axis in degrees.
    * @returns out
    * @function
    */
    public static function fromEuler( ?out : Quat, x : Float, y : Float, z : Float ) : Quat {
        if( out == null ) out = QuatTools.create();
        var halfToRad = 0.5 * Math.PI / 180.0;
        x *= halfToRad;
        y *= halfToRad;
        z *= halfToRad;

        var sx = Math.sin(x);
        var cx = Math.cos(x);
        var sy = Math.sin(y);
        var cy = Math.cos(y);
        var sz = Math.sin(z);
        var cz = Math.cos(z);

        out[0] = sx * cy * cz - cx * sy * sz;
        out[1] = cx * sy * cz + sx * cy * sz;
        out[2] = cx * cy * sz - sx * sy * cz;
        out[3] = cx * cy * cz + sx * sy * sz;

        return out;
    }
}
